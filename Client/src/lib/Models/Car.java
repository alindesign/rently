package lib.Models;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.beans.Observable;
import javafx.event.ActionEvent;
import javafx.scene.control.Alert;
import lib.Http;
import org.json.JSONObject;
import rently.Constants;
import rently.Main;

import java.io.IOException;
import java.util.Date;

public class Car {
    public static final String GAS           = "Gas";
    public static final String DIESEL        = "Diesel";
    public static final String GPL           = "GPL";
    public static final String MANUAL        = "Manual";
    public static final String AUTOMATE      = "Automate";
    public static final String SEMI_AUTOMATE = "Semi-Automate";

    private int     id;
    private int     companyId;
    private int     views;
    private int     kilometers;
    private int     rentId;
    private int     doors;
    private int     year;
    private int     rentCount = 0;
    private String  type;
    private String  image;
    private String  thumbnail;
    private String  combustible;
    private String  color;
    private String  title;
    private String  engine;
    private String  description;
    private String  companyName;
    private String  transmission;
    private double  price;
    private double  discount;
    private boolean active;
    private boolean ac;
    private boolean rentByMe  = false;
    private boolean own;
    private boolean popular;
    private Date    createdAt;
    private Date    updatedAt;

    public Car ( JSONObject o ) {
        setId( o.getInt( "id" ) );
        setOwn( o.getBoolean( "own" ) );
        setKilometers( o.getInt( "kilometers" ) );
        setCombustible( o.getString( "combustible" ) );
        setDoors( o.getInt( "doors" ) );
        setAc( o.getBoolean( "ac" ) );
        setActive( o.getBoolean( "active" ) );
        setColor( o.getString( "color" ) );
        setViews( o.getInt( "views" ) );
        setPrice( o.getDouble( "real_price" ) );
        setDiscount( o.getDouble( "discount" ) );
        setTitle( o.getString( "name" ) );
        setEngine( o.getString( "engine" ) );
        setTransmission( o.getString( "transmission" ) );
        setYear( o.getInt( "year" ) );
        setType( o.getString( "class" ) );
        setDescription( o.get( "description" ) != JSONObject.NULL ? o.getString( "description" ) : "" );
        setImage( o.get( "image" ) != JSONObject.NULL ? o.getString( "image" ) : "" );
        setThumbnail( o.get( "thumbnail" ) != JSONObject.NULL ? o.getString( "thumbnail" ) : "" );

        if ( o.get( "rent_by_me" ) != JSONObject.NULL ) {
            setRentByMe( o.getBoolean( "rent_by_me" ) );
        }
        if ( o.get( "rent_count" ) != JSONObject.NULL ) {
            setRentCount( o.getInt( "rent_count" ) );
        }
        if ( o.get( "rent_id" ) != JSONObject.NULL ) {
            setRentId( o.getInt( "rent_id" ) );
        }
    }

    public int getId () {
        return id;
    }

    public void setId ( int id ) {
        this.id = id;
    }

    public String getCombustible () {
        return combustible;
    }

    public void setCombustible ( String combustible ) {
        this.combustible = combustible;
    }

    public String getColor () {
        return color;
    }

    public void setColor ( String color ) {
        this.color = color;
    }

    public String getTitle () {
        return title;
    }

    public void setTitle ( String title ) {
        this.title = title;
    }

    public String getCompanyName () {
        return companyName;
    }

    public void setCompanyName ( String companyName ) {
        this.companyName = companyName;
    }

    public double getPrice () {
        return price - ( ( this.discount * price ) / 100.0f );
    }

    public double getRealPrice () {
        return price;
    }

    public void setPrice ( double price ) {
        this.price = price;
    }

    public int getViews () {
        return views;
    }

    public void setViews ( int views ) {
        this.views = views;
    }

    public boolean isActive () {
        return active;
    }

    public void setActive ( boolean active ) {
        this.active = active;
    }

    public String getThumbnail () {
        return thumbnail;
    }

    public void setThumbnail ( String thumbnail ) {
        this.thumbnail = thumbnail;
    }

    public Date getCreatedAt () {
        return createdAt;
    }

    public void setCreatedAt ( Date createdAt ) {
        this.createdAt = createdAt;
    }

    public Date getUpdatedAt () {
        return updatedAt;
    }

    public void setUpdatedAt ( Date updatedAt ) {
        this.updatedAt = updatedAt;
    }

    public String getTransmission () {
        return transmission;
    }

    public void setTransmission ( String transmission ) {
        this.transmission = transmission;
    }

    public double getDiscount () {
        return discount;
    }

    public void setDiscount ( double discount ) {
        this.discount = discount;
    }

    public int getCompanyId () {
        return companyId;
    }

    public void setCompanyId ( int companyId ) {
        this.companyId = companyId;
    }

    public boolean isOwn () {
        return own;
    }

    public void setOwn ( boolean own ) {
        this.own = own;
    }

    public String getStringPrice () {
        String price = String.format( "%.2f %s", getPrice(), Constants.CURRENCY );

        if ( getDiscount() > 0 ) {
            price = String.format( "%s (%.2f %s)", price, getRealPrice(), Constants.CURRENCY );
        }

        return price;
    }

    public boolean isPopular () {
        return popular;
    }

    public void setPopular ( boolean popular ) {
        this.popular = popular;
    }

    public int getKilometers () {
        return kilometers;
    }

    public void setKilometers ( int kilometers ) {
        this.kilometers = kilometers;
    }

    public int getDoors () {
        return doors;
    }

    public void setDoors ( int doors ) {
        this.doors = doors;
    }

    public int getYear () {
        return year;
    }

    public void setYear ( int year ) {
        this.year = year;
    }

    public String getType () {
        return type;
    }

    public void setType ( String type ) {
        this.type = type;
    }

    public String getImage () {
        return image;
    }

    public void setImage ( String image ) {
        this.image = image;
    }

    public String getEngine () {
        return engine;
    }

    public void setEngine ( String engine ) {
        this.engine = engine;
    }

    public boolean isAc () {
        return ac;
    }

    public void setAc ( boolean ac ) {
        this.ac = ac;
    }

    public int getRentCount () {
        return rentCount;
    }

    public void setRentCount ( int rentCount ) {
        this.rentCount = rentCount;
    }

    public boolean isRentByMe () {
        return rentByMe;
    }

    public void setRentByMe ( boolean rentByMe ) {
        this.rentByMe = rentByMe;
    }

    public void returnCar ( Observable observable ) {
        if ( getRentId() > 0 ) {
            try {
                HttpResponse< JsonNode > delete = Http.delete( String.format( "/api/rents/%d", getRentId() ) )
                        .asJson();
                if ( delete.getStatus() == 200 ) {
                    Alert alert = new Alert( Alert.AlertType.INFORMATION );
                    alert.setContentText( "Your car have been returned, thanks!" );
                    alert.show();
                    Main.setLayout( "main.fxml" );
                } else {
                    Alert alert = new Alert( Alert.AlertType.ERROR );
                    alert.setContentText( "Your car couldn't be returned, sorry!" );
                    alert.show();
                }
            } catch ( UnirestException | IOException e ) {
                e.printStackTrace();
            }
        }
    }

    public int getRentId () {
        return rentId;
    }

    public void setRentId ( int rentId ) {
        this.rentId = rentId;
    }

    public String getDescription () {
        return description;
    }

    public void setDescription ( String description ) {
        this.description = description;
    }

    public void removeCar ( ActionEvent actionEvent ) {
        System.out.println( "remove" );
        try {
            HttpResponse< JsonNode > res = Http.delete( String.format( "/api/cars/%d", getId() ) )
                    .asJson();

            if ( res.getStatus() == 200 ) {
                Alert alert = new Alert( Alert.AlertType.INFORMATION );
                alert.setContentText( "Your car have been removed!" );
                alert.show();
            }
        } catch ( UnirestException e ) {
            e.printStackTrace();
            Alert alert = new Alert( Alert.AlertType.ERROR );
            alert.setContentText( "Your car have couldn't be removed, sorry!" );
            alert.show();
        }
    }

    public void toggleCar ( ActionEvent actionEvent ) {
        System.out.println( "toggle" );
        try {
            HttpResponse< JsonNode > res = Http.post( String.format( "/api/cars/%d", getId() ) )
                    .queryString( "_method", "put" )
                    .field( "active", isActive() ? 0 : 1 )
                    .field( "_method", "put" )
                    .asJson();

            if ( res.getStatus() == 200 ) {
                Alert alert = new Alert( Alert.AlertType.INFORMATION );
                alert.setContentText( String.format( "Your car have been %s!", isActive() ? "Deactivated" : "Activated" ) );
                alert.show();
            }
        } catch ( UnirestException e ) {
            e.printStackTrace();
            Alert alert = new Alert( Alert.AlertType.ERROR );
            alert.setContentText( "Something went wrong!" );
            alert.show();
        }
    }

}
