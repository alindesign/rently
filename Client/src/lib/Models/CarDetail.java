package lib.Models;

import java.time.Year;
import java.util.Date;

public class CarDetail {
    private int     id;
    private int     make_id;
    private Make    make;
    private int     model_id;
    private Model   model;
    private int     year_id;
    private Year    year;
    private int     vehicle_id;
    private Vehicle vehicle;
    private Date    created_at;
    private Date    updated_at;
}
