package lib;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Storage {
    public static  Storage                   self = null;
    private        List< String >            content;
    private        String                    dbPath;
    private static HashMap< String, String > entries;

    public Storage () {
        if ( Storage.entries == null ) {
            Storage.entries = new HashMap<>();
        }

        dbPath = System.getProperty( "user.home" ) + File.separator + ".rently";
        File file = new File( dbPath );

        if ( !file.exists() ) {
            //noinspection ResultOfMethodCallIgnored
            file.getParentFile().mkdirs();
            try {
                //noinspection ResultOfMethodCallIgnored
                file.createNewFile();
                Files.write( Paths.get( dbPath ), "".getBytes() );
                System.out.printf( "Database created: %s", dbPath );
            } catch ( IOException e ) {
                System.out.println( "Database couldn't be created!" );
            }
        }

        try {
            this.content = Files.readAllLines( Paths.get( dbPath ) );
        } catch ( IOException e ) {
            this.content = new ArrayList<>();
        }

        readContent();

        if ( Storage.self == null ) {
            Storage.self = this;
        }
    }

    private void readContent () {
        content = content.stream().map( String::trim ).collect( Collectors.toList() );

        content.removeAll( Collections.singleton( null ) );
        content.removeAll( Collections.singleton( "" ) );

        String[] items;
        String[] splits;

        for ( String line : content ) {
            items = new String[]{ "", "" };
            splits = line.split( "=" );

            if ( splits.length > 0 ) {
                items[ 0 ] = splits[ 0 ];
                items[ 1 ] = splits.length == 1 ? "true" : splits[ 1 ];

                Storage.entries.put( items[ 0 ].trim().toUpperCase(), items[ 1 ].trim() );
                Storage.entries.put( items[ 0 ].trim().toLowerCase(), items[ 1 ].trim() );
            }
        }
    }

    public void sync () {
        StringBuilder content = new StringBuilder();

        for ( Map.Entry< String, String > entry : Storage.entries.entrySet() ) {
            String key   = entry.getKey();
            String value = entry.getValue();
            content.append( key )
                    .append( "=" )
                    .append( value )
                    .append( "\n" );
        }

        try {
            Files.write( Paths.get( dbPath ), content.toString().getBytes() );
            System.out.println( "Database SYNC Done" );
        } catch ( IOException e ) {
            System.out.printf( "Database SYNC error; [ %s ];", e.getMessage() );
        }
    }

    public void setItem ( String key, String value ) {
        entries.put( key.trim().toUpperCase(), value.trim() );
        entries.put( key.trim().toLowerCase(), value.trim() );
        sync();
    }

    public void setItem ( String key, int value ) {
        setItem( key, Utils.intToString( value ) );
    }

    public void setItem ( String key, boolean value ) {
        setItem( key, value ? "true" : "false" );
    }

    public void setItem ( String key ) {
        setItem( key, true );
    }

    public String getItem ( String key, String defaultValue ) {
        if ( hasItem( key ) ) {
            return Storage.entries.get( key );
        }

        return defaultValue;
    }

    public String getItem ( String key ) {
        return getItem( key, null );
    }

    public boolean hasItem ( String key ) {
        return Storage.entries.containsKey( key );
    }

    public void removeItem ( String key ) {
        Storage.entries.remove( key.toLowerCase() );
        Storage.entries.remove( key.toUpperCase() );
        sync();
    }
}
