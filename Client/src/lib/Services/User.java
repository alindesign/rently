package lib.Services;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import lib.Http;
import lib.Storage;
import org.json.JSONObject;
import rently.Constants;
import rently.Main;

import java.io.IOException;

public class User {
    private int    id;
    private int    imageId;
    private String role;
    private String gender;
    private String thumbnail;
    private String company;
    private String address;
    private String companyAddress;
    private String phone;
    private String email;
    private String lastName;
    private String firstName;

    public static Storage store = new Storage();

    public User () {
    }

    public User ( HttpResponse< JsonNode > req ) {
        this.fill( req );
    }

    public static void setCurrent () throws UnirestException {
        User current = User.current();

        store.setItem( "user_id", current.getId() );
        store.setItem( "user_gender", current.getGender() );
        store.setItem( "user_address", current.getAddress() == JSONObject.NULL ? "" : current.getAddress() );
        store.setItem( "user_thumbnail", current.getThumbnail() );
        store.setItem( "user_phone", current.getPhone() );
        store.setItem( "user_email", current.getEmail() );
        store.setItem( "user_last_name", current.getLastName() );
        store.setItem( "user_first_name", current.getFirstName() );
        store.setItem( "user_full_name", String.format(
                "%s %s",
                store.getItem( "user_first_name", "" ),
                store.getItem( "user_last_name", "" )
        ) );
    }

    private void fill ( HttpResponse< JsonNode > req ) {
        JSONObject data = req.getBody().getObject();

        setId( data.getInt( "id" ) );
        setGender( data.getString( "gender" ) );
        setThumbnail( data.get( "thumbnail" ) != JSONObject.NULL ? ( String ) data.get( "thumbnail" ) : Constants.getDefaultUserImage() );
        setPhone( data.getString( "phone" ) );
        setAddress( data.get( "address" ) == JSONObject.NULL ? "" : data.getString( "address" ) );
        setEmail( data.getString( "email" ) );
        setLastName( data.getString( "last_name" ) );
        setFirstName( data.getString( "first_name" ) );
    }

    public static User current () throws UnirestException {
        HttpResponse< JsonNode > req = Http.post( "api/auth/current" ).asJson();

        return new User( req );
    }

    public static void logout () throws IOException {
        store.removeItem( "authenticated" );
        store.removeItem( "jwt" );
        Main.setLayout( "login.fxml" );
    }

    public int getId () {
        return id;
    }

    public void setId ( int id ) {
        this.id = id;
    }

    public int getImageId () {
        return imageId;
    }

    public void setImageId ( int imageId ) {
        this.imageId = imageId;
    }

    public String getRole () {
        return role;
    }

    public void setRole ( String role ) {
        this.role = role;
    }

    public String getGender () {
        if ( gender == null ) {
            return "Unset";
        }

        return gender;
    }

    public void setGender ( String gender ) {
        this.gender = gender;
    }

    public String getPhone () {
        return phone;
    }

    public void setPhone ( String phone ) {
        this.phone = phone;
    }

    public String getEmail () {
        return email;
    }

    public void setEmail ( String email ) {
        this.email = email;
    }

    public String getLastName () {
        return lastName;
    }

    public void setLastName ( String lastName ) {
        this.lastName = lastName;
    }

    public String getFirstName () {
        return firstName;
    }

    public void setFirstName ( String firstName ) {
        this.firstName = firstName;
    }

    public String getThumbnail () {
        return thumbnail != null ? thumbnail : Constants.getDefaultUserImage();
    }

    public void setThumbnail ( String thumbnail ) {
        this.thumbnail = thumbnail;
    }

    public String getCompany () {
        return company;
    }

    public void setCompany ( String company ) {
        this.company = company;
    }

    public String getCompanyAddress () {
        return companyAddress;
    }

    public void setCompanyAddress ( String companyAddress ) {
        this.companyAddress = companyAddress;
    }

    public String getAddress () {
        return address;
    }

    public void setAddress ( String address ) {
        this.address = address;
    }
}
