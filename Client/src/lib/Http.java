package lib;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.request.HttpRequest;
import com.mashape.unirest.request.HttpRequestWithBody;
import rently.Constants;

public class Http {
    public static Storage store = new Storage();

    public static HttpRequestWithBody post ( String uri ) {
        HttpRequestWithBody req = Unirest.post( createUri( uri ) );

        return ( HttpRequestWithBody ) request( "POST", req );
    }

    public static HttpRequestWithBody put ( String uri ) {
        HttpRequestWithBody req = Unirest.put( createUri( uri ) );

        return ( HttpRequestWithBody ) request( "PUT", req );
    }


    public static HttpRequest get ( String uri ) {
        HttpRequest req = Unirest.get( createUri( uri ) );

        return request( "GET", req );
    }

    public static int REQUEST_COUNTER = 0;

    private static HttpRequest request ( String method, HttpRequest req ) {
        req.header( "X-Requested-With", "XMLHttpRequest" );
        req.queryString( "_t", Http.REQUEST_COUNTER );
        Http.REQUEST_COUNTER += 1;

        Boolean auth  = Boolean.valueOf( store.getItem( "authenticated", "false" ) );
        String  token = store.getItem( "JWT" );

        if ( auth && token != null ) {
            req.header( "Authorization", String.format( "Bearer %s", token ) );
            System.out.println( "Add token" );
        }

        System.out.printf( "Request %s %s\n", method, req.getUrl() );

        return req;
    }

    private static String createUri ( String uri ) {
        return Constants.url( uri );
    }

    public static HttpRequest delete ( String uri ) {
        HttpRequest req = Unirest.delete( createUri( uri ) );

        return request( "Delete", req );
    }
}
