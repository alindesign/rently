package rently;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import lib.Http;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ResourceBundle;

public class CreateCarController implements Initializable {
    private MakeItem    selectedMake    = null;
    private ModelItem   selectedModel   = null;
    private YearItem    selectedYear    = null;
    private VehicleItem selectedVehicle = null;

    @FXML
    private ImageView               carImage;
    @FXML
    private Button                  uploadImage;
    @FXML
    private ComboBox< MakeItem >    carBrand;
    @FXML
    private ComboBox< ModelItem >   carModel;
    @FXML
    private ComboBox< YearItem >    carYear;
    @FXML
    private ComboBox< VehicleItem > carVehicle;
    @FXML
    private ComboBox< String >      carCombustible;
    @FXML
    private TextField               carColor;
    @FXML
    private TextField               carKilometers;
    @FXML
    private ComboBox< Integer >     carDoors;
    @FXML
    private TextArea                carDescription;
    @FXML
    private CheckBox                carAc;
    @FXML
    private TextField               carPrice;
    @FXML
    private TextField               carDiscount;
    @FXML
    private Button                  saveCar;

    public  FileChooser fileChooser   = new FileChooser();
    private File        selectedImage = null;

    @Override public void initialize ( URL location, ResourceBundle resources ) {
        carBrand.setDisable( true );
        carBrand.setOpacity( 0.40 );

        carModel.setDisable( true );
        carModel.setOpacity( 0.40 );

        carYear.setDisable( true );
        carYear.setOpacity( 0.40 );

        carVehicle.setDisable( true );
        carVehicle.setOpacity( 0.40 );

        carDoors.setPromptText( "Select value" );
        carDoors.setItems( FXCollections.observableArrayList( 3, 5 ) );

        carCombustible.setPromptText( "Select value" );
        carCombustible.setItems( FXCollections.observableArrayList(
                "Diesel",
                "Gas",
                "LPG",
                "Electricity"
        ) );

        try {
            new FillCombo<>( "/api/vehicle/makes", "makes", carBrand, MakeItem.class );

            carBrand.valueProperty().addListener( ( observable, oldValue, newValue ) -> {
                this.selectedMake = newValue;
                if ( ( oldValue != null && oldValue.getValue() != newValue.getValue() ) || carModel.getItems().size() == 0 ) {
                    try {
                        new FillCombo<>( String.format( "/api/vehicle/%d/models",
                                selectedMake.getValue()
                        ), "models", carModel, ModelItem.class );
                    } catch ( UnirestException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e ) {
                        e.printStackTrace();
                    }
                }
            } );

            carModel.valueProperty().addListener( ( observable, oldValue, newValue ) -> {
                this.selectedModel = newValue;
                if ( ( oldValue != null && oldValue.getValue() != newValue.getValue() ) || carYear.getItems().size() == 0 ) {
                    try {
                        new FillCombo<>( String.format( "/api/vehicle/%d/%d/years",
                                selectedMake.getValue(),
                                selectedModel.getValue()
                        ), "years", carYear, YearItem.class );
                    } catch ( UnirestException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e ) {
                        e.printStackTrace();
                    }
                }
            } );

            carYear.valueProperty().addListener( ( observable, oldValue, newValue ) -> {
                this.selectedYear = newValue;
                if ( ( oldValue != null && oldValue.getValue() != newValue.getValue() ) || carVehicle.getItems().size() == 0 ) {
                    try {
                        new FillCombo<>( String.format( "/api/vehicle/%d/%d/%d/vehicles",
                                selectedMake.getValue(),
                                selectedModel.getValue(),
                                selectedYear.getValue()
                        ), "vehicles", carVehicle, VehicleItem.class );
                    } catch ( UnirestException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e ) {
                        e.printStackTrace();
                    }
                }
            } );

            carVehicle.valueProperty().addListener( ( observable, oldValue, newValue ) -> {
                this.selectedVehicle = newValue;
                System.out.println( newValue.getItem() );
            } );
        } catch ( UnirestException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e ) {
            e.printStackTrace();
        }
    }

    @FXML
    private void openFileDialog ( ActionEvent e ) {
        fileChooser.setTitle( "Open Image File" );
        File image = fileChooser.showOpenDialog( Main.stage );
        if ( image != null ) {
            setImage( image );
        }
    }

    private void setImage ( File image ) {
        selectedImage = image;
        carImage.setImage( new Image( String.valueOf( image.toURI() ) ) );
        carImage.setFitHeight( 150.0 );
        carImage.setFitWidth( 200.0 );
    }

    @FXML
    private void submitCar ( ActionEvent e ) throws UnirestException, IOException {
        if ( carDiscount.getText().isEmpty() ) {
            carDiscount.setText( "0" );
        }

        if ( selectedVehicle == null ) {
            error( "Vehicle is required!" );
        } else if ( carPrice.getText().isEmpty() ) {
            error( "Price required" );
        } else if ( carKilometers.getText().isEmpty() ) {
            error( "Kilometers required" );
        } else if ( selectedImage == null ) {
            error( "Image required" );
        } else if ( Float.valueOf( carDiscount.getText() ) > 100.0 || Float.valueOf( carDiscount.getText() ) < 0 ) {
            error( "Discount isn't between 0 and 100" );
        } else {
            HttpResponse< String > req = Http.post( "/api/cars" )
                    .field( "image", selectedImage )
                    .field( "vehicle_id", selectedVehicle.getValue() )
                    .field( "color", carColor.getText() )
                    .field( "combustible", carCombustible.getValue() )
                    .field( "doors", carDoors.getValue() )
                    .field( "description", carDescription.getText() )
                    .field( "ac", carAc.isSelected() )
                    .field( "price", carPrice.getText() )
                    .field( "discount", carDiscount.getText() )
                    .field( "kilometers", carKilometers.getText() )
                    .asString();

            if ( req.getStatus() == 200 ) {
                Main.setLayout( "main.fxml" );
            } else {
                error( "Something went wrong, vehicle can't be saved!" );
            }
        }
    }

    private void error ( String message ) {
        Alert alert = new Alert( Alert.AlertType.ERROR );
        alert.setTitle( "Error Validation" );
        alert.setContentText( message );

        alert.show();
    }

}
