package rently;

import org.json.JSONObject;

public class MakeItem {
    private JSONObject item;

    public MakeItem () {
        this( new JSONObject() );
    }

    public MakeItem ( Object item ) {
        this( ( JSONObject ) item );
    }

    public MakeItem ( JSONObject item ) {
        this.item = item;
    }

    public int getValue () {
        return item.getInt( "id" );
    }

    public String toString () {
        return item.getString( "name" );
    }
}
