package rently;

import org.json.JSONObject;

public class YearItem {
    private JSONObject item;

    public YearItem () {
        this( new JSONObject() );
    }

    public YearItem ( Object item ) {
        this( ( JSONObject ) item );
    }

    public YearItem ( JSONObject item ) {
        this.item = item;
    }

    public int getValue () {
        return item.getInt( "id" );
    }

    public String toString () {
        return String.valueOf( item.getInt( "year" ) );
    }
}
