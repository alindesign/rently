package rently;

import javafx.collections.ListChangeListener;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import lib.Storage;

import java.io.IOException;
import java.util.List;

public class Layout extends BorderPane {

    @FXML
    private VBox content;

    @FXML
    private Button reloadAction;

    @FXML
    private ImageView openMenu;

    @FXML
    private ImageView profileImage;

    @FXML
    private ImageView headerProfile;

    @FXML
    private VBox sidebar;

    @FXML
    private Label userFullName2;

    @FXML
    private Label userFullName1;

    private Storage store = new Storage();
    static  Layout  self;

    public Layout () {
        FXMLLoader fxmlLoader = new FXMLLoader( this.getClass().getResource( "Layout.fxml" ) );
        fxmlLoader.setRoot( this );
        fxmlLoader.setController( this );
        fxmlLoader.setClassLoader( getClass().getClassLoader() );

        try {
            fxmlLoader.load();
        } catch ( IOException exception ) {
            throw new RuntimeException( exception );
        }
    }

    @FXML
    public void initialize () {
        DropShadow shadow = new DropShadow( 20, Color.rgb( 0, 0, 0, 0.40 ) );
        Circle     clip   = new Circle( 32 );
        Circle     clip2  = new Circle( 16 );

        clip.setCenterX( 32 );
        clip.setCenterY( 32 );

        clip2.setCenterX( 16 );
        clip2.setCenterY( 16 );

        profileImage.setClip( clip );
        profileImage.setEffect( shadow );

        headerProfile.setClip( clip2 );
        headerProfile.setEffect( shadow );

        profileImage.setImage( new Image( store.getItem( "user_thumbnail", Constants.getDefaultUserImage() ) ) );
        headerProfile.setImage( new Image( store.getItem( "user_thumbnail", Constants.getDefaultUserImage() ) ) );

        userFullName1.setText( store.getItem( "user_full_name", "" ) );
        userFullName2.setText( store.getItem( "user_full_name", "" ) );

        getChildren().addListener( ( ListChangeListener< Node > ) c -> {
            while ( c.next() ) {
                if ( c.wasAdded() ) {
                    List< ? extends Node > added = c.getAddedSubList();
                    for ( Node node : added ) {
                        content.getChildren().add( node );
                    }
                }
            }
        } );

        self = this;
    }

    boolean visible = false;

    @FXML
    public void openMenuAction ( MouseEvent event ) {
        visible = !visible;
        String icon = visible ? "back" : "menu";
        double size = visible ? 270.0 : 0.0;

        sidebar.setVisible( visible );
        sidebar.setPrefWidth( size );

        openMenu.setImage( new Image( String.valueOf( getClass().getResource( String.format( "../assets/icons/%s.png", icon ) ) ) ) );
    }

    @FXML
    public void setLogout ( ActionEvent event ) throws IOException {
        store.removeItem( "authenticated" );
        store.removeItem( "jwt" );
        Main.setLayout( "login.fxml" );
    }

    @FXML
    public void reloadLayout ( ActionEvent event ) throws IOException {
        Main.reload();
    }

    @FXML
    public void setSearch ( ActionEvent event ) throws IOException {
        Main.setLayout( "main.fxml" );
    }

    @FXML
    public void setHistory ( ActionEvent event ) throws IOException {
        Main.setLayout( "history.fxml" );
    }

    @FXML
    public void setMyCars ( ActionEvent event ) throws IOException {
        Main.setLayout( "myCars.fxml" );
    }

    @FXML
    public void setSettings ( ActionEvent event ) throws IOException {
        Main.setLayout( "settings.fxml" );
    }

    @FXML
    public void setPromotions ( ActionEvent event ) throws IOException {
        Main.setLayout( "promotions.fxml" );
    }

    @FXML
    public void setAbout ( ActionEvent event ) throws IOException {
        Main.setLayout( "about.fxml" );
    }

    @FXML
    public void setTermsConditions ( ActionEvent event ) throws IOException {
        Main.setLayout( "termsConditions.fxml" );
    }

    public VBox getContent () {
        return content;
    }
}
