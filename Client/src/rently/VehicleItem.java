package rently;

import org.json.JSONObject;

public class VehicleItem {
    private JSONObject item;

    public VehicleItem () {
        this( new JSONObject() );
    }

    public VehicleItem ( Object item ) {
        this( ( JSONObject ) item );
    }

    public VehicleItem ( JSONObject item ) {
        this.item = item;
    }

    public int getValue () {
        return item.getInt( "id" );
    }

    public String toString () {
        return item.getString( "name" );
    }

    public JSONObject getItem () {
        return item;
    }
}
