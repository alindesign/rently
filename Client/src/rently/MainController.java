package rently;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.FlowPane;
import lib.Http;
import lib.Models.Car;
import lib.Storage;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ResourceBundle;

public class MainController implements Initializable {

    @FXML
    public FlowPane itemList;

    @FXML
    private TextField searchQuery;

    @FXML
    private ComboBox< String > combustible;

    @FXML
    private ComboBox< String > transmission;
    private Storage            store = new Storage();

    @Override
    public void initialize ( URL location, ResourceBundle resources ) {
        searchQuery.setPromptText( "Search by text..." );

        combustible.setItems( FXCollections.observableArrayList(
                "All",
                "Diesel",
                "Gas",
                "LPG",
                "Electricity"
        ) );
        combustible.setTooltip( new Tooltip( "Select the combustible." ) );
        combustible.setValue( store.getItem( "filter_combustible", "All" ) );

        transmission.setItems( FXCollections.observableArrayList(
                "All",
                "4-Speed Automatic",
                "5-Speed Manual",
                "5-Speed Automatic",
                "6-Speed Manual",
                "8-Speed Automated Manual",
                "CVT",
                "6-Speed Automatic",
                "9-Speed Automatic",
                "9-Speed Automated Manual",
                "7-Speed Automatic",
                "7-Speed Automated Manual",
                "6-Speed Automated Manual",
                "3-Speed Automatic",
                "4-Speed Manual",
                "8-Speed Automatic",
                "1-Speed Direct Drive",
                "Auto(A1)",
                "3-Speed Manual",
                "7-Speed Manual",
                "1-Speed Automatic",
                "4-Speed Manual Doubled",
                "Auto(L4)",
                "Auto(L3)",
                "Auto(A8)",
                "5-Speed Automated Manual"
        ) );
        transmission.setTooltip( new Tooltip( "Select the transmission." ) );
        transmission.setValue( store.getItem( "filter_transmission", "All" ) );

        combustible.valueProperty().addListener( this::changed );
        transmission.valueProperty().addListener( this::changed );

        searchQuery.focusedProperty().addListener( ( arg0, oldPropertyValue, newPropertyValue ) -> {
            if ( !newPropertyValue ) {
                try {
                    fetch();
                } catch ( UnirestException e ) {
                    itemList.getChildren().add( new Label( "No cars" ) );
                }
            }
        } );

        try {
            fetch();
        } catch ( UnirestException e ) {
            itemList.getChildren().add( new Label( "No cars" ) );
        }
    }

    private void fetch () throws UnirestException {
        itemList.getChildren().clear();
        HttpResponse< JsonNode > cars = Http.get( "/api/cars" )
                .queryString( "q", searchQuery.getText() )
                .queryString( "combustible", combustible.getValue().toLowerCase().trim() )
                .queryString( "transmission", transmission.getValue().toLowerCase().trim() )
                .asJson();

        if ( cars.getStatus() == 200 ) {
            JSONArray carList = cars.getBody().getObject().getJSONArray( "data" );
            if ( carList.length() > 0 ) {
                Car car;
                for ( Object o : carList ) {
                    car = new Car( ( JSONObject ) o );
                    itemList.getChildren().add( new CarItem( car ) );
                }
            } else {
                itemList.getChildren().add( new Label( "No cars" ) );
            }
        } else {
            itemList.getChildren().add( new Label( "No cars" ) );
        }
    }

    private void changed ( ObservableValue< ? extends String > observable, String oldValue, String newValue ) {
        if ( !oldValue.equals( newValue ) ) {
            try {
                fetch();
            } catch ( UnirestException e ) {
                itemList.getChildren().add( new Label( "No cars" ) );
            }
        }
    }
}
