package rently;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.FlowPane;
import lib.Http;
import lib.Models.Car;
import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;
import java.util.ResourceBundle;

public class HistoryController implements Initializable {
    @FXML
    public FlowPane itemList;


    @SuppressWarnings("Duplicates")
    @Override
    public void initialize ( URL location, ResourceBundle resources ) {

        HttpResponse< JsonNode > cars = null;
        try {
            cars = Http.get( "/api/cars" )
                    .queryString( "history", 1 )
                    .asJson();
            if ( cars.getStatus() == 200 ) {
                JSONArray carList = cars.getBody().getObject().getJSONArray( "data" );
                if ( carList.length() > 0 ) {
                    Car car;
                    for ( Object o : carList ) {
                        car = new Car( ( JSONObject ) o );
                        itemList.getChildren().add( new CarListItem( car, "history" ) );
                    }
                } else {
                    noCars();
                }
            } else {
                noCars();
            }
        } catch ( UnirestException e ) {
            noCars();
        }
    }

    private void noCars () {
        itemList.getChildren().clear();
        itemList.getChildren().add( new Label( "No cars" ) );
    }
}
