package rently;

import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

public class Constants {
    public static final String CURRENCY = "RON";
    public static final String APP_URL  = "http://rently.test";

    public static Font getFont ( int size ) {
        return Font.font( "Segoe UI", FontWeight.MEDIUM, size );
    }

    public static Font getFont () {
        return Constants.getFont( 14 );
    }

    public static String url () {
        return Constants.url( "" );
    }

    public static String url ( String s ) {
        if ( s.startsWith( "/" ) ) {
            s = s.substring( 1 );
        }

        return String.format( "%s/%s", Constants.APP_URL, s );
    }

    public static String getDefaultUserImage () {
        return Constants.class.getResource( "../assets/icons/user_icon.png" ).toExternalForm();
    }
}
