package rently;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.ComboBox;
import lib.Http;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;

public class FillCombo< T > {
    HttpResponse< JsonNode > request;

    public FillCombo ( String uri, String key, ComboBox< T > field, Class< T > ClassItem ) throws UnirestException, NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        request = Http.get( uri ).asJson();
        JSONArray           makesArray = request.getBody().getObject().getJSONArray( key );
        ObservableList< T > list       = FXCollections.observableArrayList( );

        for ( Object item : makesArray ) {
            list.add( ClassItem.getConstructor( JSONObject.class ).newInstance( ( JSONObject ) item ) );
        }

        field.setItems( list );
        field.setDisable( false );
        field.setOpacity( 1 );
        field.setPromptText( "Select value" );
    }
}
