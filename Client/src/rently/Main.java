package rently;

import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import lib.Services.User;
import lib.Storage;

import java.io.IOException;

public class Main extends Application {
    public static Storage store = new Storage();

    public static Stage  stage;
    public static Parent root;
    public static Scene  scene;
    public static Main   self;
    public static String sLayout;

    @Override
    public void start ( Stage primaryStage ) throws Exception {
        Main.self = this;
        Main.stage = primaryStage;

        Main.stage.setMinWidth( 1200.0 );
        Main.stage.setMinHeight( 720.0 );

        setTitle( "Rently" );
        setScene();
        setLayout();
    }

    private void setLayout () throws IOException, UnirestException {
        String  layout        = Main.store.getItem( "LAST_VIEW", "main.xml" );
        boolean authenticated = Boolean.valueOf( store.getItem( "authenticated" ) );

        if ( !authenticated ) {
            layout = "login.fxml";
        } else {
            User.setCurrent();
        }

        setLayout( layout );
    }

    private void setScene () throws IOException {
        int width  = Integer.valueOf( Main.store.getItem( "LAST_WIDTH", "1200" ) );
        int height = Integer.valueOf( Main.store.getItem( "LAST_HEIGHT", "720" ) );

        Main.root = FXMLLoader.load( Main.class.getResource( "splash-screen.fxml" ) );

        Main.scene = new Scene( Main.root, width, height );
        Main.stage.setScene( Main.scene );
        Main.stage.show();
        Main.root.requestFocus();
    }

    public static void setLayout ( String layout, int width, int height ) throws IOException {
        System.out.printf( "Change layout: %s\n", layout );

        Main.sLayout = layout;
        Main.root = FXMLLoader.load( Main.class.getResource( layout ) );

        Main.scene.setRoot( Main.root );
        Main.store.setItem( "LAST_VIEW", layout );
        Main.store.setItem( "LAST_WIDTH", width );
        Main.store.setItem( "LAST_HEIGHT", height );
    }

    public static void setLayout ( String layout ) throws IOException {
        int width  = Integer.valueOf( Main.store.getItem( "LAST_WIDTH", "1200" ) );
        int height = Integer.valueOf( Main.store.getItem( "LAST_HEIGHT", "720" ) );

        Main.setLayout( layout, width, height );
    }

    public static void reload () throws IOException {
        System.out.printf( "Reload Layout %s\n", Main.sLayout );
        Main.setLayout( Main.sLayout );
    }

    public static void setTitle ( String title ) {
        Main.stage.setTitle( title );
    }

    public static void main ( String[] args ) {
        launch( args );
    }
}
