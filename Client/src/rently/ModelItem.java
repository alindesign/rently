package rently;

import org.json.JSONObject;

public class ModelItem {
    private JSONObject item;

    public ModelItem () {
        this( new JSONObject() );
    }

    public ModelItem ( Object item ) {
        this( ( JSONObject ) item );
    }

    public ModelItem ( JSONObject item ) {
        this.item = item;
    }

    public int getValue () {
        return item.getInt( "id" );
    }

    public String toString () {
        return item.getString( "name" );
    }
}
