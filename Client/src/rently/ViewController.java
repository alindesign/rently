package rently;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.Insets;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import lib.Http;
import lib.Models.Car;
import lib.Storage;

import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;

import static java.time.temporal.ChronoUnit.DAYS;

public class ViewController implements Initializable {
    public static int carId = -1;
    @FXML
    private VBox items;
    @FXML
    private ImageView carImage;

    private IconItem carKilometers = new IconItem("../assets/icons/settings.png");
    private IconItem carYear = new IconItem("../assets/icons/calendar_today.png");
    private IconItem carCombustible = new IconItem("../assets/icons/local_gas_station.png");
    private IconItem carDoors = new IconItem("../assets/icons/directions_car.png");
    private IconItem carAC = new IconItem("../assets/icons/directions_car.png", "AC");
    private IconItem carColor = new IconItem("../assets/icons/color.png");
    private IconItem carTransmission = new IconItem("../assets/icons/settings.png");
    private IconItem carEngine = new IconItem("../assets/icons/directions_car.png");

    @FXML
    private VBox features;
    @FXML
    private TextField rentAddress;
    @FXML
    private DatePicker rentStartDate;
    @FXML
    private DatePicker rentEndDate;
    @FXML
    private Label viewsLabel;
    @FXML
    private Label priceLabel;
    @FXML
    private Button editButton;
    @FXML
    private Button applyButton;

    @FXML
    private Label carTitle;

    @FXML
    private Label carType;

    @FXML
    private VBox textHolder;

    @FXML
    private Label carDescription;

    private Car car;
    private LocalDate startDate;
    private LocalDate endDate;
    private int days = 1;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        String savedCarId = Storage.self.getItem("view_id", "");

        if (carId == -1 && !savedCarId.isEmpty()) {
            carId = Integer.valueOf(savedCarId);
        }

        if (carId == -1) {
            try {
                redirect();
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            Storage.self.setItem("view_id", carId);
            fetchCar();
        }
    }

    @SuppressWarnings("Duplicates")
    private void fetchCar() {
        try {
            HttpResponse<JsonNode> req = Http.get(String.format("/api/cars/%d", carId)).asJson();
            if (req.getStatus() == 200) {
                car = new Car(req.getBody().getObject().getJSONObject("data"));
                setDetails();
            }
        } catch (UnirestException e) {
            try {
                Main.setLayout("main.fxml");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    private void setDetails() {

        if (!car.getImage().isEmpty()) {
            carImage.setImage(new Image(car.getImage()));
        }

        carTitle.setText(car.getTitle());
        carType.setText(car.getType());

        if (!car.getDescription().isEmpty()) {
            carDescription.setText(car.getDescription());
        } else {
            carDescription.setText("");
            carDescription.setVisible(false);
            VBox.setMargin(carDescription, new Insets(0));
        }

        carKilometers.setText(String.valueOf(car.getKilometers()));
        carCombustible.setText(car.getCombustible());
        carYear.setText(String.valueOf(car.getYear()));
        carDoors.setText(String.valueOf(car.getDoors()));
        carColor.setText(car.getColor());
        carTransmission.setText(car.getTransmission());
        carEngine.setText(car.getEngine());
        viewsLabel.setText(String.format("%d views", car.getViews()));

        calculatePrice();

        rentStartDate.setOnAction((actionEvent) -> {
            startDate = rentStartDate.getValue();
            calculatePrice();
        });

        rentEndDate.setOnAction((actionEvent) -> {
            endDate = rentEndDate.getValue();
            calculatePrice();
        });

        items.getChildren().addAll(carKilometers, carYear, carCombustible, carDoors);

        if (car.isAc()) {
            items.getChildren().add(carAC);
        }

        items.getChildren().addAll(carColor, carTransmission, carEngine);
    }

    private void calculatePrice() {
        if (startDate != null && endDate != null) {
            days = (int) DAYS.between(startDate, endDate);
        }

        if (days < 1) {
            days = 1;
        }

        priceLabel.setText(
                String.format("%.2f RON, %d %s", car.getPrice() * (double) days, days, ((days != 1) ? "days" : "day"))
        );
    }

    public void rent(ActionEvent actionEvent) {

        try {
            if (rentAddress.getText().isEmpty()) {
                alert("Address is required!", Alert.AlertType.ERROR);
            } else if (rentStartDate.getValue() == null) {
                alert("Start Date is required", Alert.AlertType.ERROR);
            } else if (rentEndDate.getValue() == null) {
                alert("End Date is required", Alert.AlertType.ERROR);
            } else {
                HttpResponse<JsonNode> req = Http.post("/api/rents")
                        .field("car_id", carId)
                        .field("address", rentAddress.getText())
                        .field("start_date", rentStartDate.getValue())
                        .field("end_date", rentEndDate.getValue())
                        .asJson();

                System.out.println(req);
                System.out.println(req.getStatus());
                if (req.getStatus() == 200) {
                    alert("Your care it's ready, Thank you!", Alert.AlertType.INFORMATION);
                    Main.setLayout("main.fxml");
                } else if (req.getStatus() == 404) {
                    alert("This car can't be found or it's already rent, Sorry!", Alert.AlertType.WARNING);
                } else {
                    alert("Something went wrong!", Alert.AlertType.ERROR);
                }
            }
        } catch (UnirestException e) {
            e.printStackTrace();
            alert("Something went wrong!", Alert.AlertType.ERROR);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void alert(String message, Alert.AlertType type) {
        Alert alert = new Alert(type);
        alert.setHeaderText(message);
        alert.show();
    }

    public void goHome(ActionEvent actionEvent) throws IOException {
        redirect();
    }

    public void redirect() throws IOException {
        Main.setLayout("main.fxml");
        carId = -1;
        Storage.self.removeItem("view_id");
    }

    public void editRedirect(ActionEvent actionEvent) {
        try {
            EditController.carId = car.getId();
            Main.setLayout("edit.fxml");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
