package rently;


import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class ImageUploader extends VBox {

    public ImageView imageView = new ImageView();
    public Button    button    = new Button( "Upload" );

    public ImageUploader () {
        Circle     clip   = new Circle( 70 );
        DropShadow shadow = new DropShadow( 20, Color.rgb( 0, 0, 0, 0.20 ) );
        clip.setCenterX( 70 );
        clip.setCenterY( 70 );

        imageView.prefWidth( 140 );
        imageView.prefHeight( 140 );
        imageView.setFitWidth( 140 );
        imageView.setFitHeight( 140 );
        imageView.setClip( clip );
        imageView.setEffect( shadow );


        button.getStyleClass().add( "purple-button-small" );
        VBox.setMargin( button, new Insets( 14, 0, 0, 0 ) );

        getChildren().addAll( imageView, button );
        setAlignment( Pos.TOP_CENTER );
        setPrefWidth( 140.0 );
        setMaxWidth( 140.0 );
    }

    public final StringProperty imageProperty () {
        if ( image == null ) {
            image = new SimpleStringProperty( this, "image", "" );
        }
        return image;
    }

    private StringProperty image;

    public final void setImage ( String value ) {
        imageProperty().setValue( value );
        imageView.setImage( new Image( value ) );
    }

    public final String getImage () {
        return image == null ? "" : image.getValue();
    }

    public void onAction ( Object openFileDialog ) {

    }
}
