package rently;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import lib.Models.Car;

import java.io.IOException;
import java.net.URL;

public class CarItem extends VBox {
    private VBox     root        = new VBox();
    private FlowPane imageHolder = new FlowPane();

    private VBox   controls = new VBox();
    private Label  label    = new Label();
    private Label  popular  = new Label( "Popular" );
    private VBox   items    = new VBox();
    private HBox   buttons  = new HBox();
    private Button button   = new Button( "View" );
    private Button edit     = new Button( "Edit" );
    private Button ret      = new Button( "Return" );

    private Car car;

    CarItem ( Car car ) {
        this.car = car;
        initialize();
    }

    private double width = 293.0;

    @SuppressWarnings("Duplicates") @FXML
    public void initialize () {
        if ( !car.isActive() && !car.isRentByMe() ) {
            setDisable( true );
            setOpacity( 0.6 );
        }
        imageHolder.getStyleClass().add( "image-holder" );
        imageHolder.setPrefWidth( width - 16.0 );
        imageHolder.setMinWidth( width - 16.0 );
        imageHolder.setMinHeight( 210.0 );

        setWidth( width );
        setMinWidth( width );

        items.getStyleClass().add( "icon-items-padding" );
        items.getChildren().addAll(
                new IconItem( "../assets/icons/local_gas_station.png", car.getCombustible() ),
                new IconItem( "../assets/icons/settings.png", car.getTransmission() ),
                new IconItem( "../assets/icons/attach_money.png", car.getStringPrice() )
        );

        root.getStyleClass().add( "car-list-item" );
        root.setPadding( new Insets( 8, 8, 8, 9 ) );
        root.setEffect( new DropShadow( 20.0, new Color( 0, 0, 0, 0.20 ) ) );

        HBox.setMargin( button, new Insets( 0, 8, 0, 0 ) );
        HBox.setMargin( edit, new Insets( 0, 8, 0, 0 ) );

        button.getStyleClass().add( "purple-button-small" );
        edit.getStyleClass().add( "purple-button-small" );
        ret.getStyleClass().add( "purple-button-small" );

        button.setOnAction( event -> {
            ViewController.carId = car.getId();

            try {
                Main.setLayout( "view.fxml" );
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        } );

        edit.setOnAction( event -> {
            EditController.carId = car.getId();

            try {
                Main.setLayout( "edit.fxml" );
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        } );

        label.setText( car.getTitle() );
        label.setPrefWidth( width - 30.0 );
        label.setTextAlignment( TextAlignment.LEFT );
        label.setPadding( new Insets( 8, 0, 8, 0 ) );
        label.setFont( Constants.getFont( 18 ) );

        if ( car.isPopular() ) {
            popular.setFont( Constants.getFont() );
            popular.setTextFill( Color.valueOf( "#009688" ) );
            popular.setPadding( new Insets( 0, 8, 0, 0 ) );
            buttons.getChildren().add( popular );
        }

        buttons.getChildren().add( button );
        buttons.setAlignment( Pos.CENTER_RIGHT );

        if ( car.isOwn() ) {
            buttons.getChildren().add( edit );
        }

        if ( car.isRentByMe() ) {
            ret.onActionProperty().addListener( car::returnCar );
            buttons.getChildren().add( ret );
        }

        controls.getChildren().addAll(
                label,
                items,
                buttons
        );

        root.getChildren().addAll( imageHolder, controls );

        getChildren().add( root );
        setPadding( new Insets( 8 ) );

        setImage( car.getThumbnail() );
    }

    public void setImage ( String image ) {
        if ( !image.isEmpty() ) {
            imageHolder.setStyle( String.format(
                    "-fx-background-image: url(%s);",
                    image
            ) );
        }
    }

    public void setImage ( URL resource ) {
        setImage( resource.getPath() );
    }
}
