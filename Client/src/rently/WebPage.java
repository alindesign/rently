package rently;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;

public class WebPage extends VBox {

    private WebView webView = new WebView();

    public WebPage () {
        VBox.setVgrow( webView, Priority.ALWAYS );
        getChildren().add( webView );
    }

    public final StringProperty urlProperty () {
        if ( url == null ) {
            url = new SimpleStringProperty( this, "url", "" );
        }
        return url;
    }

    private StringProperty url;

    public final void setUrl ( String value ) {
        urlProperty().setValue( value );
        webView.getEngine().load( Constants.url( value ) );
    }

    public final String getUrl () {
        return url == null ? "" : url.getValue();
    }
}
