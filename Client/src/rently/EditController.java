package rently;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.body.MultipartBody;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import lib.Http;
import lib.Models.Car;
import lib.Storage;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.URL;
import java.util.ResourceBundle;

@SuppressWarnings("ALL")
public class EditController implements Initializable {
    public static int         carId           = -1;
    private       MakeItem    selectedMake    = null;
    private       ModelItem   selectedModel   = null;
    private       YearItem    selectedYear    = null;
    private       VehicleItem selectedVehicle = null;

    @FXML
    private ImageView               carImage;
    @FXML
    private Button                  uploadImage;
    @FXML
    private ComboBox< MakeItem >    carBrand;
    @FXML
    private ComboBox< ModelItem >   carModel;
    @FXML
    private ComboBox< YearItem >    carYear;
    @FXML
    private ComboBox< VehicleItem > carVehicle;
    @FXML
    private ComboBox< String >      carCombustible;
    @FXML
    private TextField               carColor;
    @FXML
    private TextField               carKilometers;
    @FXML
    private ComboBox< Integer >     carDoors;
    @FXML
    private TextArea                carDescription;
    @FXML
    private CheckBox                carAc;
    @FXML
    private TextField               carPrice;
    @FXML
    private TextField               carDiscount;
    @FXML
    private Button                  saveCar;

    private File selectedImage = null;
    private Car  car;

    @Override public void initialize ( URL location, ResourceBundle resources ) {
        String savedCarId = Storage.self.getItem( "edit_id", "" );

        if ( carId == -1 && !savedCarId.isEmpty() ) {
            carId = Integer.valueOf( savedCarId );
        }

        if ( carId == -1 ) {
            try {
                redirect();
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        } else {
            Storage.self.setItem( "edit_id", carId );
            init();
        }
    }

    public void redirect () throws IOException {
        Main.setLayout( "main.fxml" );
        carId = -1;
        Storage.self.removeItem( "edit_id" );
    }

    public void init () {
        fetchCar();

        carBrand.setDisable( true );
        carBrand.setOpacity( 0.40 );

        carModel.setDisable( true );
        carModel.setOpacity( 0.40 );

        carYear.setDisable( true );
        carYear.setOpacity( 0.40 );

        carVehicle.setDisable( true );
        carVehicle.setOpacity( 0.40 );

        carDoors.setPromptText( "Select value" );
        carDoors.setItems( FXCollections.observableArrayList( 3, 5 ) );

        carCombustible.setPromptText( "Select value" );
        carCombustible.setItems( FXCollections.observableArrayList(
                "Diesel",
                "Gas",
                "LPG",
                "Electricity"
        ) );

        try {
            new FillCombo<>( "/api/vehicle/makes", "makes", carBrand, MakeItem.class );

            carBrand.valueProperty().addListener( ( observable, oldValue, newValue ) -> {
                this.selectedMake = newValue;
                if ( ( oldValue != null && oldValue.getValue() != newValue.getValue() ) || carModel.getItems().size() == 0 ) {
                    try {
                        new FillCombo<>( String.format( "/api/vehicle/%d/models",
                                selectedMake.getValue()
                        ), "models", carModel, ModelItem.class );
                    } catch ( UnirestException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e ) {
                        e.printStackTrace();
                    }
                }
            } );

            carModel.valueProperty().addListener( ( observable, oldValue, newValue ) -> {
                this.selectedModel = newValue;
                if ( ( oldValue != null && oldValue.getValue() != newValue.getValue() ) || carYear.getItems().size() == 0 ) {
                    try {
                        new FillCombo<>( String.format( "/api/vehicle/%d/%d/years",
                                selectedMake.getValue(),
                                selectedModel.getValue()
                        ), "years", carYear, YearItem.class );
                    } catch ( UnirestException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e ) {
                        e.printStackTrace();
                    }
                }
            } );

            carYear.valueProperty().addListener( ( observable, oldValue, newValue ) -> {
                this.selectedYear = newValue;
                if ( ( oldValue != null && oldValue.getValue() != newValue.getValue() ) || carVehicle.getItems().size() == 0 ) {
                    try {
                        new FillCombo<>( String.format( "/api/vehicle/%d/%d/%d/vehicles",
                                selectedMake.getValue(),
                                selectedModel.getValue(),
                                selectedYear.getValue()
                        ), "vehicles", carVehicle, VehicleItem.class );
                    } catch ( UnirestException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e ) {
                        e.printStackTrace();
                    }
                }
            } );

            carVehicle.valueProperty().addListener( ( observable, oldValue, newValue ) -> {
                this.selectedVehicle = newValue;
            } );
        } catch ( UnirestException | NoSuchMethodException | IllegalAccessException | InvocationTargetException | InstantiationException e ) {
            e.printStackTrace();
        }
    }

    private void fetchCar () {
        try {
            HttpResponse< JsonNode > req = Http.get( String.format( "/api/cars/%d", carId ) ).asJson();
            if ( req.getStatus() == 200 ) {
                car = new Car( req.getBody().getObject().getJSONObject( "data" ) );
                setDetails();
            }
        } catch ( UnirestException e ) {
            try {
                Main.setLayout( "main.fxml" );
            } catch ( IOException e1 ) {
                e1.printStackTrace();
            }
        }
    }

    private void setDetails () {
        setImage( car.getImage() );
        carCombustible.setValue( car.getCombustible() );
        carDoors.setValue( car.getDoors() );
        carColor.setText( car.getColor() );
        carKilometers.setText( String.valueOf( car.getKilometers() ) );
        carDescription.setText( car.getDescription() );
        carAc.setSelected( car.isAc() );
        carPrice.setText( String.valueOf( car.getRealPrice() ) );
        carDiscount.setText( String.valueOf( car.getDiscount() ) );
    }

    private void setImage ( String image ) {
        carImage.setImage( new Image( image ) );
        carImage.setFitHeight( 150.0 );
        carImage.setFitWidth( 200.0 );
    }

    @FXML
    private void saveCar ( ActionEvent e ) throws UnirestException, IOException {
        if ( carDiscount.getText().isEmpty() ) {
            carDiscount.setText( "0" );
        }

        if ( Float.valueOf( carDiscount.getText() ) > 100.0 || Float.valueOf( carDiscount.getText() ) < 0 ) {
            error( "Discount isn't between 0 and 100" );
        } else {
            MultipartBody req = Http.post( String.format( "/api/cars/%d", car.getId() ) )
                    .queryString( "_method", "put" )
                    .field( "_method", "put" )
                    .field( "ac", carAc.isSelected() );

            if ( selectedImage != null ) {
                req = req.field( "image", selectedImage );
            }
            if ( selectedVehicle != null ) {
                req = req.field( "vehicle_id", selectedVehicle.getValue() );
            }
            if ( !carColor.getText().isEmpty() ) {
                req = req.field( "color", carColor.getText() );
            }
            if ( !carCombustible.getValue().isEmpty() ) {
                req = req.field( "combustible", carCombustible.getValue() );
            }
            if ( carDoors.getValue() != null ) {
                req = req.field( "doors", carDoors.getValue() );
            }
            if ( !carDescription.getText().isEmpty() ) {
                req = req.field( "description", carDescription.getText() );
            }
            if ( !carPrice.getText().isEmpty() ) {
                req = req.field( "price", carPrice.getText() );
            }
            if ( !carDiscount.getText().isEmpty() ) {
                req = req.field( "discount", carDiscount.getText() );
            }
            if ( !carKilometers.getText().isEmpty() ) {
                req = req.field( "kilometers", carKilometers.getText() );
            }

            HttpResponse< String > res = req.asString();
            if ( res.getStatus() == 200 ) {
                alert( "Car has been updated!", Alert.AlertType.INFORMATION );
            } else {
                error( "Something went wrong, vehicle can't be saved!" );
            }
        }
    }

    private void error ( String message ) {
        alert( message, Alert.AlertType.ERROR );
    }

    private void alert ( String message, Alert.AlertType type ) {
        Alert alert = new Alert( type );
        alert.setHeaderText( message );
        alert.show();
    }
}
