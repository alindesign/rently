package rently;

import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;

public class IconItem extends HBox {

    @FXML
    private ImageView imageView = new ImageView();

    @FXML
    private Label label = new Label();

    public IconItem () {
        initialize();
    }

    private void initialize () {
        getStyleClass().add( "icon-item" );

        imageView.getStyleClass().add( "icon-item--image" );
        imageView.setFitHeight( 24 );
        imageView.setFitWidth( 24 );
        imageView.setOpacity( 0.3 );

        label.getStyleClass().add( "icon-item--text" );

        this.getChildren().addAll( imageView, label );
        setAlignment( Pos.CENTER_LEFT );
    }

    public IconItem ( String image ) {
        this( image, "" );
    }

    public IconItem ( String image, String label ) {
        setText( label );
        setImage( image );
        initialize();
    }

    public final StringProperty textProperty () {
        if ( text == null ) {
            text = new SimpleStringProperty( this, "text", "" );
        }
        return text;
    }

    private StringProperty text;

    public final void setText ( String value ) {
        textProperty().setValue( value );
        this.label.setText( value );
    }

    public final String getText () {
        return text == null ? "" : text.getValue();
    }

    public final StringProperty imageProperty () {
        if ( image == null ) {
            image = new SimpleStringProperty( this, "image", "" );
        }
        return image;
    }

    private StringProperty image;

    public final void setImage ( String value ) {
        imageProperty().setValue( value );
        this.imageView.setImage(
                new Image(
                        getClass().getResource( value ).toExternalForm()
                )
        );
    }

    public final String getImage () {
        return image == null ? "" : image.getValue();
    }
}
