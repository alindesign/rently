package rently;

import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.effect.DropShadow;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import lib.Models.Car;

import java.io.IOException;
import java.net.URL;

public class CarListItem extends HBox {
    private FlowPane imageHolder = new FlowPane();

    private HBox  root     = new HBox();
    private VBox  controls = new VBox();
    private HBox  buttons  = new HBox();
    private Label title    = new Label();
    private Label views    = new Label();

    private Button deactivate = new Button( "Deactivate" );
    private Button view       = new Button( "View" );
    private Button remove     = new Button( "Remove" );
    private Button ret        = new Button( "Return" );
    private Button edit       = new Button( "Edit" );

    private HBox items = new HBox();

    private int     carId;
    private boolean popular;
    private String  name;
    private Double  price;
    private String  image;
    private Car     car;
    private String  type;

    CarListItem ( Car car, String type ) {
        setType( type );
        fill( car );
        initialize();
    }

    public CarListItem ( Car car ) {
        this( car, "own" );
    }

    private void fill ( Car car ) {
        this.car = car;
        setCarId( car.getId() );
        setName( car.getTitle() );
        setPrice( car.getPrice() );
        setImage( car.getThumbnail() );
    }

    private double width       = 900.0;
    private double imageWidth  = 240;
    private double imageHeight = 200;

    @SuppressWarnings("Duplicates")
    @FXML
    public void initialize () {
        if ( !car.isActive() && ( !car.isRentByMe() && !car.isOwn() ) ) {
            setDisable( true );
            setOpacity( 0.6 );
        }

        imageHolder.getStyleClass().add( "image-holder" );
        imageHolder.setPrefWidth( imageWidth );
        imageHolder.setMinWidth( imageWidth );
        imageHolder.setMinHeight( imageHeight );

        setWidth( width );
        setMinWidth( width );

        if ( !car.isActive() ) {
            deactivate.setText( "Activate" );
        } else {
            deactivate.setText( "Deactivate" );
        }

        remove.setOnAction( ( event ) -> {
            car.removeCar( event );
            setVisible( false );
            maxHeight( 0 );
        } );
        deactivate.setOnAction( ( event ) -> {
            car.toggleCar( event );
            car.setActive( !car.isActive() );
            if ( !car.isActive() ) {
                deactivate.setText( "Activate" );
            } else {
                deactivate.setText( "Deactivate" );
            }
        } );

        setInsets( new Button[]{
                deactivate,
                remove,
                edit,
                ret,
                view
        } );

        title.setText( car.getTitle() );
        title.getStyleClass().add( "car-list-item--title" );

        views.setText( String.format( "Views %d", car.getViews() ) );
        views.getStyleClass().add( "car-list-item--views" );

        boolean isOwn = ( getType().contains( "own" ) || car.isOwn() );

        if ( isOwn ) {
            buttons.getChildren().addAll( deactivate, remove, edit );
        }

        if ( car.isRentByMe() ) {
            ret.onActionProperty().addListener( car::returnCar );
            buttons.getChildren().addAll( ret );
        }

        buttons.setPadding( new Insets( 14, 0, 0, 0 ) );

        view.setOnAction( event -> {
            ViewController.carId = car.getId();

            try {
                Main.setLayout( "view.fxml" );
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        } );

        edit.setOnAction( event -> {
            EditController.carId = car.getId();

            try {
                Main.setLayout( "view.fxml" );
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        } );
        buttons.getChildren().add( view );

        items.getStyleClass().add( "icon-items" );
        items.getChildren().addAll(
                new IconItem( "../assets/icons/local_gas_station.png", car.getCombustible() ),
                new IconItem( "../assets/icons/settings.png", car.getTransmission() ),
                new IconItem( "../assets/icons/attach_money.png", car.getStringPrice() )
        );

        controls.getChildren().addAll( title, views, items, buttons );
        HBox.setMargin( controls, new Insets( 6, 0, 0, 14 ) );

        root.setPrefWidth( width );
        root.setMinWidth( width );
        root.setEffect( new DropShadow( 20.0, new Color( 0, 0, 0, 0.20 ) ) );
        root.setPadding( new Insets( 8 ) );

        root.getStyleClass().add( "car-list-item" );
        root.getChildren().addAll( imageHolder, controls );

        HBox.setMargin( this, new Insets( 8, 0, 0, 0 ) );
        setPadding( new Insets( 8 ) );
        getChildren().add( root );
    }

    private void setInsets ( Button[] buttons ) {
        for ( Button button : buttons ) {
            button.getStyleClass().add( "purple-button-small" );
            HBox.setMargin( button, new Insets( 0, 8, 0, 0 ) );
        }
    }

    public void setImage ( String image ) {
        this.image = image;

        if ( !image.isEmpty() ) {
            imageHolder.setStyle( String.format(
                    "-fx-background-image: url(%s);",
                    image
            ) );
        }
    }

    public void setImage ( URL resource ) {
        setImage( resource.getPath() );
    }

    public int getCarId () {
        return carId;
    }

    public void setCarId ( int carId ) {
        this.carId = carId;
    }

    public boolean isPopular () {
        return popular;
    }

    public void setPopular ( boolean popular ) {
        this.popular = popular;
    }

    public String getName () {
        return name;
    }

    public void setName ( String name ) {
        this.name = name;
    }

    public Double getPrice () {
        return price;
    }

    public void setPrice ( Double price ) {
        this.price = price;
    }

    public String getType () {
        return type;
    }

    public void setType ( String type ) {
        this.type = type;
    }
}
