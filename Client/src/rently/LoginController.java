package rently;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.FlowPane;
import lib.Http;
import lib.Services.User;
import lib.Storage;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;

public class LoginController {
    @FXML
    public FlowPane loading;

    @FXML
    public TextField emailField;

    @FXML
    public PasswordField passwordField;

    @FXML
    public Hyperlink forgetPasswordLink;

    @FXML
    public FlowPane errorsHolder;

    @FXML
    public FlowPane errors;

    private Storage store = new Storage();

    @FXML
    public void requestLogin ( MouseEvent mouseEvent ) throws IOException, UnirestException {
        loading.setVisible( true );

        System.out.printf( "Send login Request [%s, %s]\n", emailField.getText(), passwordField.getText() );

        HttpResponse< JsonNode > res = Http.post( "/api/auth/login" )
                .field( "email", emailField.getText() )
                .field( "password", passwordField.getText() )
                .asJson();

        System.out.printf( "Send Login request end with %d\n", res.getStatus() );

        switch ( res.getStatus() ) {
            case 422:
                errorsHolder.getChildren().clear();
                JSONObject data = res.getBody().getObject().getJSONObject( "errors" );

                for ( String key : data.keySet() ) {
                    JSONArray errorsList = data.getJSONArray( key );
                    for ( int i = 0; i < errorsList.length(); i++ ) {
                        errorAlert( errorsList.get( i ).toString() );
                    }
                }

                errors.setVisible( true );
                loading.setVisible( false );
                break;
            case 401:
                System.out.println( res.getBody().getObject() );
                errorsHolder.getChildren().clear();
                this.errorAlert( res.getBody().getObject().getString( "error" ) );
                errors.setVisible( true );
                loading.setVisible( false );
                break;
            case 200:
                store.setItem( "authenticated", "true" );
                store.setItem( "jwt", res.getBody().getObject().getString( "access_token" ) );
                User.setCurrent();
                Main.setLayout( "main.fxml" );
        }
    }

    private void errorAlert ( String message ) {
        System.out.println( message );
        Label err = new Label( message );
        err.setStyle( "-fx-text-fill: #ff3510" );
        errorsHolder.getChildren().add( err );
    }

    @FXML
    public void signUpRedirect ( MouseEvent mouseEvent ) {
        Main.self.getHostServices().showDocument( "http://rently.test/sign-up" );
    }

    @FXML
    public void forgetPasswordRedirect ( MouseEvent mouseEvent ) {
        Main.self.getHostServices().showDocument( "http://rently.test/forget-password" );
    }

    @FXML
    public void closeErrorsView ( MouseEvent mouseEvent ) {
        errors.setVisible( false );
    }
}
