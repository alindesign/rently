package rently;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.body.MultipartBody;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.layout.HBox;
import lib.Http;
import lib.Services.User;

import java.net.URL;
import java.util.ResourceBundle;

public class SettingsController implements Initializable {
    @FXML
    public ChoiceBox< String > user_gender;

    @FXML
    public TextField user_first_name;

    @FXML
    public TextField user_last_name;

    @FXML
    public TextField user_phone;

    @FXML
    public TextField user_address;
    @FXML
    public HBox      form;

    @FXML
    public ImageUploader imageUploader;
    public TextField     user_old_password;
    public TextField     user_password;
    public TextField     user_confirm_password;

    User   user;
    String gender;

//    public  FileChooser fileChooser = new FileChooser();
//    private File        userImage;

    @Override public void initialize ( URL location, ResourceBundle resources ) {
        try {
            user = User.current();

            gender = user.getGender();
            gender = String.format( "%s%s", gender.substring( 0, 1 ).toUpperCase(), gender.substring( 1 ) );

            System.out.println( gender );
            user_gender.setItems( FXCollections.observableArrayList(
                    "Unset",
                    "Male",
                    "Female"
            ) );
            user_gender.setTooltip( new Tooltip( "Select the Gender." ) );
            user_gender.setValue( gender );

            user_first_name.setText( user.getFirstName() );
            user_last_name.setText( user.getLastName() );
            user_phone.setText( user.getPhone() );
            user_address.setText( user.getAddress() );
        } catch ( UnirestException e ) {
            e.printStackTrace();
        }
        imageUploader.setImage( user.getThumbnail() );
        imageUploader.button.setVisible( false );
//        imageUploader.button.setOnAction( this::openFileDialog );
    }

//    private void openFileDialog ( ActionEvent actionEvent ) {
//        fileChooser.setTitle( "Open Image File" );
//        File image = fileChooser.showOpenDialog( Main.stage );
//        if ( image != null ) {
//            setImage( image );
//        }
//    }

    public void submitData ( ActionEvent actionEvent ) throws UnirestException {
        MultipartBody body = Http.post( String.format( "/api/users/%d", user.getId() ) )
                .field( "_method", "put" );

//        if ( userImage != null ) {
//            body = body.field( "image", userImage );
//        }

        if ( !gender.equals( user_gender.getValue() ) ) {
            body = body.field( "gender", user_gender.getValue() );
            gender = user_gender.getValue();
        }

        if ( !user.getFirstName().equals( user_first_name.getText() ) ) {
            body = body.field( "first_name", user_first_name.getText() );
        }

        if ( !user.getLastName().equals( user_last_name.getText() ) ) {
            body = body.field( "last_name", user_last_name.getText() );
        }

        if ( !user.getPhone().equals( user_phone.getText() ) ) {
            body = body.field( "phone", user_phone.getText() );
        }

        if ( !user.getAddress().equals( user_address.getText() ) ) {
            body = body.field( "address", user_address.getText() );
        }

        HttpResponse< JsonNode > req = body.asJson();

        Alert alert;
        if ( req.getStatus() == 200 ) {
            alert = new Alert( Alert.AlertType.INFORMATION );
            alert.setTitle( "Successfully!" );
            alert.setContentText( "User info successfully updated" );
        } else {
            alert = new Alert( Alert.AlertType.WARNING );
            alert.setTitle( "Error!" );
            alert.setContentText( "Something went wrong, User info can't be saved!" );
        }

        alert.show();
        User.setCurrent();
    }

//    private void setImage ( File image ) {
//        userImage = image;
//        imageUploader.setImage( String.valueOf( image.toURI() ) );
//    }
}
