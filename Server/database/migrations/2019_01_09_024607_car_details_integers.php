<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CarDetailsIntegers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('car_details', function ( Blueprint $table ) {
            $table->unsignedInteger('make_id')->change();
            $table->unsignedInteger('model_id')->change();
            $table->unsignedInteger('year_id')->change();
            $table->unsignedInteger('vehicle_id')->change();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
