<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CarFields extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table("cars", function ( Blueprint $table ) {
            $table->string('combustible')->nullable()->change();
            $table->string('color')->nullable()->change();
            $table->string('views')->default(0)->change();
            $table->unsignedInteger("kilometers")->nullable()->change();
            $table->smallInteger("doors")->nullable()->change();
            $table->boolean("ac")->nullable()->change();
            
            $table->unsignedInteger('image_id')->nullable()->change();
            $table->unsignedInteger('user_id')->nullable()->change();
            $table->unsignedInteger('company_id')->nullable()->change();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
