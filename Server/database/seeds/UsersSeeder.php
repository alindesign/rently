<?php

use App\Gender;
use App\Role;
use App\User;
use Illuminate\Database\Seeder;

class UsersSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $email = 'alinus@rently.com';
        
        if ( !User::whereEmail($email)->first() ) {
            $user = new User();
            
            $user->first_name = 'Dumitrana';
            $user->last_name  = 'Alinus';
            $user->email      = $email;
            $user->phone      = '0766865341';
            $user->gender     = Gender::MALE;
            $user->password   = bcrypt('secret');
            $user->role_id    = Role::ADMIN;
            
            $user->save();
            $user->markEmailAsVerified();
        }
        
        factory(App\User::class, 10)->create();
    }
}
