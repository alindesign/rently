<?php

use App\Role;
use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        foreach ( Role::getRoles() as $name ) {
            $role = new Role();
            
            $role->name      = $name;
            $role->localized = "roles." . strtolower($name);
            
            $role->save();
        }
    }
}
