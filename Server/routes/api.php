<?php

use Illuminate\Support\Facades\Route;

Route::group([
    'middleware' => 'api',
], function () {
    Route::group([
        'prefix' => 'auth'
    ], function () {
        Route::post('login', 'AuthController@login');
        Route::post('logout', 'AuthController@logout');
        Route::post('reload', 'AuthController@refresh');
        Route::post('current', 'AuthController@me');
    });
    
    Route::resource('users', 'UserController');
    Route::resource('images', 'ImageController');
    Route::resource('cars', 'CarController');
    Route::resource('rents', 'RentController');
    
    Route::get('vehicle/makes', [
        'uses' => '\Gerardojbaez\Vehicle\Controllers\MakesController@makes',
        'as'   => 'api.vehicles.makes'
    ]);
    
    Route::get('vehicle/{make}/models', [
        'uses' => '\Gerardojbaez\Vehicle\Controllers\ModelsController@models',
        'as'   => 'api.vehicles.models'
    ]);
    
    Route::get('vehicle/{make}/{model}/years', [
        'uses' => '\Gerardojbaez\Vehicle\Controllers\ModelYearsController@years',
        'as'   => 'api.vehicles.years'
    ]);
    
    Route::get('vehicle/{make}/{model}/{year}/vehicles', [
        'uses' => '\Gerardojbaez\Vehicle\Controllers\VehiclesController@vehicles',
        'as'   => 'api.vehicles.vehicles'
    ]);
    
    Route::get('vehicle/{vehicle}/vehicle', [
        'uses' => '\Gerardojbaez\Vehicle\Controllers\VehiclesController@vehicle',
        'as'   => 'api.vehicles.vehicle'
    ]);
});
