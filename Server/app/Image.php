<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property int            id
 * @property int            user_id
 * @property string         name
 * @property string         description
 * @property string         filename
 * @property string         path
 * @property int            size
 * @property string         mime_type
 * @property string         extension
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 * @property string         hash
 * @property mixed          thumbnail
 */
class Image extends Model
{
    //
}
