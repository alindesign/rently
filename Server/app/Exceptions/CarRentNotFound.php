<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class CarRentNotFound extends Exception
{
    protected $code = Response::HTTP_NOT_FOUND;
    protected $message = "Car rent not found";
}
