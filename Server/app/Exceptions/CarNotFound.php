<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class CarNotFound extends Exception
{
    protected $code = Response::HTTP_NOT_FOUND;
    protected $message = 'Car not found.';
}
