<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class UnauthorizedAccess extends Exception
{
    protected $code = Response::HTTP_UNAUTHORIZED;
    protected $message = 'Unauthorized Access!';
}
