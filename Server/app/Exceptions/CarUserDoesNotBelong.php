<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class CarUserDoesNotBelong extends Exception
{
    protected $code = Response::HTTP_UNAUTHORIZED;
    protected $message = "Unauthorized action";
}
