<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class CarAlreadyRent extends Exception
{
    protected $code = Response::HTTP_NOT_FOUND;
    protected $message = "Car already rent!";
}
