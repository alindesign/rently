<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class ImageNotFound extends Exception
{
    protected $code = Response::HTTP_NOT_FOUND;
    protected $message = 'Image not found.';
}
