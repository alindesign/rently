<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class ImageIsTooBig extends Exception
{
    protected $message = 'Image is too big.';
    protected $code = Response::HTTP_FORBIDDEN;
}
