<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class UserNotFound extends Exception
{
    protected $message = 'User not found!';
    protected $code = Response::HTTP_NOT_FOUND;
}
