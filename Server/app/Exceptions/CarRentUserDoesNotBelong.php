<?php

namespace App\Exceptions;

use Exception;
use Illuminate\Http\Response;

class CarRentUserDoesNotBelong extends Exception
{
    protected $code = Response::HTTP_UNAUTHORIZED;
    protected $message = "Unauthorized action";
}
