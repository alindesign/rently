<?php

namespace App\Jobs\Car;

use App\Car;
use App\CarDetail;
use App\Exceptions\CarNotFound;
use App\Exceptions\CarUserDoesNotBelong;
use App\Http\Resources\Car as CarResource;
use App\Traits\UserUtils;
use Gerardojbaez\Vehicle\Models\Vehicle;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class Delete implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, UserUtils;
    
    /**
     * @var bool
     */
    private $resource;
    private $id;
    
    /**
     * Create a new job instance.
     *
     * @param                                         $id
     * @param bool                                    $resource
     */
    public function __construct( $id, $resource = true )
    {
        $this->resource = $resource;
        $this->id       = $id;
    }
    
    /**
     * Execute the job.
     *
     * @return \App\Car|\App\Http\Resources\Car
     * @throws \App\Exceptions\CarNotFound
     * @throws \App\Exceptions\CarUserDoesNotBelong
     */
    public function handle()
    {
        $user = $this->currentUser();
        $car  = Car::find($this->id);
        
        if ( !$car ) {
            throw new CarNotFound();
        }
        
        if ( $car->user_id !== $user->id ) {
            throw new CarUserDoesNotBelong();
        }
        
        $car->delete();
        
        return $car;
    }
}
