<?php

namespace App\Jobs\Car;

use App\Car;
use App\CarDetail;
use App\Http\Resources\Car as CarResource;
use App\Jobs\Images\Create as CreateImage;
use App\Jobs\Price\Create as CreatePrice;
use App\Traits\UserUtils;
use Gerardojbaez\Vehicle\Models\Vehicle;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Support\Facades\Log;

class Create implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, UserUtils;
    
    /**
     * @var \Illuminate\Foundation\Http\FormRequest
     */
    private $request;
    /**
     * @var bool
     */
    private $resource;
    
    /**
     * Create a new job instance.
     *
     * @param \Illuminate\Foundation\Http\FormRequest $request
     * @param bool                                    $resource
     */
    public function __construct( FormRequest $request, $resource = true )
    {
        $this->request  = $request;
        $this->resource = $resource;
    }
    
    /**
     * Execute the job.
     *
     * @return \App\Car|\App\Http\Resources\Car
     */
    public function handle()
    {
        $user = $this->currentUser();
        
        $request = $this->request;
        
        $detail = new CarDetail();
        $car    = new Car();
        
        $car->active  = true;
        $car->views   = 0;
        $car->user_id = $user->id;
        
        if ( $user->isCompany() ) {
            $car->company_id = $user->company_id;
        }
        
        if ( $request->has('image') ) {
            $image         = dispatch_now(new CreateImage($request, $user->id));
            $car->image_id = $image->id;
        }
        
        $car->color       = $request->get('color');
        $car->combustible = $request->get('combustible');
        $car->doors       = intval($request->get('doors'));
        $car->description = $request->get('description');
        $car->ac          = filter_var($request->get('ac'), FILTER_VALIDATE_BOOLEAN);
        $car->kilometers  = intval($request->get('kilometers'));
    
        if ( $request->has('vehicle_id') ) {
            $vehicle = Vehicle::find(intval($request->get('vehicle_id')));
            
            $detail->vehicle_id = $vehicle->id;
            $detail->make_id    = $vehicle->make_id;
            $detail->model_id   = $vehicle->model_id;
            $detail->year_id    = $vehicle->year_id;
        }
        
        $detail->save();
        
        $car->car_detail_id = $detail->id;
        $car->save();
        
        dispatch_now(new CreatePrice($car, floatval($request->get('price')), floatval($request->get('discount'))));
        
        if ( $this->resource ) {
            return new CarResource($car);
        }
        
        return $car;
    }
}
