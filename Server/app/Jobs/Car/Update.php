<?php

namespace App\Jobs\Car;

use App\Car;
use App\CarDetail;
use App\Exceptions\CarNotFound;
use App\Exceptions\CarUserDoesNotBelong;
use App\Http\Resources\Car as CarResource;
use App\Traits\UserUtils;
use Gerardojbaez\Vehicle\Models\Vehicle;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class Update implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels, UserUtils;
    
    /**
     * @var \Illuminate\Foundation\Http\FormRequest
     */
    private $request;
    /**
     * @var bool
     */
    private $resource;
    private $id;
    
    /**
     * Create a new job instance.
     *
     * @param \Illuminate\Foundation\Http\FormRequest $request
     * @param bool                                    $resource
     */
    public function __construct( FormRequest $request, $id, $resource = true )
    {
        $this->request  = $request;
        $this->resource = $resource;
        $this->id       = $id;
    }
    
    /**
     * Execute the job.
     *
     * @return \App\Car|\App\Http\Resources\Car
     * @throws \App\Exceptions\CarNotFound
     * @throws \App\Exceptions\CarUserDoesNotBelong
     */
    public function handle()
    {
        $user    = $this->currentUser();
        $request = $this->request;
        $car     = Car::find($this->id);
        
        if ( !$car ) {
            throw new CarNotFound();
        }
        
        if ( $car->user_id !== $user->id ) {
            throw new CarUserDoesNotBelong();
        }
        
        if ( $request->has('active') ) {
            $car->active = $request->get('active');
        }
        
        if ( $request->has("color") ) {
            $car->color = $request->get('color');
        }
        
        if ( $request->has("combustible") ) {
            $car->combustible = $request->get('combustible');
        }
        
        if ( $request->has("doors") ) {
            $car->doors = intval($request->get('doors'));
        }
        
        if ( $request->has("description") ) {
            $car->description = $request->get('description');
        }
        
        if ( $request->has("ac") ) {
            $car->ac = filter_var($request->get('ac'), FILTER_VALIDATE_BOOLEAN);
        }
        
        if ( $request->has("kilometers") ) {
            $car->kilometers = intval($request->get('kilometers'));
        }
        
        if ( $request->has('vehicle_id') ) {
            $detail = $car->detail;
            
            if ( !$detail ) {
                $detail = new CarDetail();
            }
            
            $vehicle = Vehicle::find(intval($request->get('vehicle_id')));
            
            $detail->vehicle_id = $vehicle->id;
            $detail->make_id    = $vehicle->make_id;
            $detail->model_id   = $vehicle->model_id;
            $detail->year_id    = $vehicle->year_id;
            
            $detail->save();
        }
        
        $car->save();
        
        $savePrice = false;
        
        if ( $request->has("price") ) {
            $car->price->price = floatval($request->get('price'));
            $savePrice         = true;
        }
        
        if ( $car->price && $request->has("discount") ) {
            $car->price->discount = floatval($request->get('discount'));
            $savePrice            = true;
        }
        
        if ( $savePrice ) {
            $car->price->save();
        }
        
        if ( $this->resource ) {
            return new CarResource($car);
        }
        
        return $car;
    }
}
