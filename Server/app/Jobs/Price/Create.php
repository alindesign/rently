<?php

namespace App\Jobs\Price;

use App\Car;
use App\Price;
use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

class Create implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;
    /**
     * @var \App\Car
     */
    private $car;
    /**
     * @var float
     */
    private $price;
    /**
     * @var float
     */
    private $discount;
    /**
     * @var string
     */
    private $description;
    
    /**
     * Create a new job instance.
     *
     * @param \App\Car $car
     * @param float    $price
     * @param float    $discount
     * @param string   $description
     */
    public function __construct( Car $car, float $price, float $discount = 0, string $description = '' )
    {
        $this->car         = $car;
        $this->price       = $price;
        $this->discount    = $discount;
        $this->description = $description;
    }
    
    /**
     * Execute the job.
     *
     * @return \App\Price
     */
    public function handle()
    {
        $price = new Price();
        
        $price->car_id      = $this->car->id;
        $price->price       = floatval($this->price);
        $price->discount    = floatval($this->discount);
        $price->description = $this->description;
        $price->active      = true;
        
        $price->save();
        
        return $price;
    }
}
