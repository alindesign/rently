<?php

namespace App\Jobs\Images;

use App\Exceptions\ImageNotFound;
use App\Http\Resources\Image as ImageResource;
use App\Repositories\ImageRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class Read
{
    use Dispatchable, Queueable;
    
    /**
     * @var bool
     */
    private $resource;
    private $id;
    /**
     * @var null
     */
    private $hash;
    
    /**
     * ReadAll constructor.
     * @param      $id
     * @param null $hash
     * @param bool $resource
     */
    public function __construct( $id, $hash = null, $resource = true )
    {
        $this->resource = $resource;
        $this->hash     = $hash;
        $this->id       = $id;
    }
    
    /**
     * @param \App\Repositories\ImageRepository $repository
     * @return \App\Http\Resources\Image|\App\Image
     * @throws \App\Exceptions\ImageNotFound
     */
    public function handle( ImageRepository $repository )
    {
        $image = null;
        
        if ( $this->id ) {
            $image = $repository->getById($this->id);
        } else {
            if ( $this->hash ) {
                $image = $repository->getByHash($this->hash);
            }
        }
        
        if ( !$image ) {
            throw new ImageNotFound();
        }
        
        if ( $this->resource ) {
            return new ImageResource($image);
        }
        
        return $image;
    }
}
