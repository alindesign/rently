<?php

namespace App\Jobs\Images;

use App\Exceptions\ImageNotFound;
use App\Exceptions\UserNotFound;
use App\Repositories\ImageRepository;
use App\Traits\UserUtils;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class Destroy
{
    use Dispatchable, Queueable, UserUtils;
    /**
     * @var int
     */
    private $id;
    /**
     * @var int
     */
    private $user_id;
    /**
     * @var bool
     */
    private $resource;
    
    /**
     * @param      $id
     * @param      $user_id
     * @param bool $resource
     */
    public function __construct( $id, $user_id, $resource = true )
    {
        $this->id       = $id;
        $this->user_id  = $user_id;
        $this->resource = $resource;
    }
    
    /**
     * @param \App\Repositories\ImageRepository $repository
     * @return \App\Http\Resources\Image|\App\Image|\App\User|null
     * @throws \App\Exceptions\ImageNotFound
     * @throws \App\Exceptions\UserNotFound
     * @throws \Exception
     */
    public function handle( ImageRepository $repository )
    {
        $current = $this->currentUser();
        $uid     = $this->user_id ? $this->user_id : (
        $current ? $current->id : null
        );
        
        if ( $uid ) {
            throw new UserNotFound();
        }
        
        $image = $repository->getByIdAndUser($this->id, $uid);
        
        if (
            !$image ||
            (
                !$current->isAdmin() && ( !$uid || $uid !== $image->user_id )
            )
        ) {
            throw new ImageNotFound();
        }
        
        if ( $image->path ) {
            @unlink($image->path);
        }
        
        if ( $image->thumbnail ) {
            @unlink($image->thumbnail);
        }
        
        $image->delete();
        
        return $image;
    }
}
