<?php

namespace App\Jobs\Images;

use App\Exceptions\ImageIsTooBig;
use App\Http\Resources\Image as ImageResource;
use App\Image;
use App\Traits\UserUtils;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Foundation\Http\FormRequest;

class Create
{
    use Dispatchable, Queueable, UserUtils;
    /**
     * @var \Illuminate\Http\Request
     */
    private $request;
    /**
     * @var bool
     */
    private $resource;
    /**
     * @var int
     */
    private $user_id;
    
    /**
     * @var string
     */
    private $store;
    
    /**
     * Create a new job instance.
     *
     * @param \Illuminate\Foundation\Http\FormRequest $request
     * @param int                                     $user_id
     * @param bool                                    $resource
     */
    public function __construct( FormRequest $request, int $user_id, $resource = true )
    {
        
        $this->request  = $request;
        $this->resource = $resource;
        $this->user_id  = $user_id;
        $this->store    = storage_path('uploads/images');
        
        if ( !file_exists($this->store) ) {
            mkdir($this->store, 0777, true);
        }
    }
    
    /**
     * @return \App\Http\Resources\Image|\App\Image
     * @throws \App\Exceptions\ImageIsTooBig
     */
    public function handle()
    {
        $dest = $this->store;
        $file = $this->request->file('image');
        $uid  = $this->user_id;
        
        if ( !$uid ) {
            $user = $this->currentUser();
            
            if ( $user ) {
                $uid = $user->id;
            }
        }
        
        $image = new Image();
        
        $image->name        = $this->request->get('name') ?? $file->getClientOriginalName();
        $image->description = $this->request->get('description') ?? '';
        $image->filename    = $file->getClientOriginalName();
        $image->size        = $file->getSize();
        $image->mime_type   = $file->getClientMimeType();
        $image->extension   = $file->getClientOriginalExtension();
        
        if ( $image->size > 2097152 ) {
            throw new ImageIsTooBig();
        }
        
        $hash = implode('-', [
            $image->id,
            $image->name,
            $image->size,
            $image->user_id,
            $image->mime_type,
            substr($image->description, 0, 254)
        ]);
        $hash = sha1($hash) . '.' . $image->extension;
        
        if ( $uid ) {
            $image->user_id = $uid;
            $dest           .= '/u-' . $uid . '/';
        } else {
            $dest .= '/u-anon/';
        }
        
        $image->hash = $hash;
        $image->path = $dest . $hash;
        $file->move($dest, $hash);
        
        
        $thumbnail = \Intervention\Image\Facades\Image::make($image->path)
                                                      ->widen(300, function ( $constraint ) {
                                                          $constraint->upsize();
                                                      });
        
        $image->thumbnail = $dest . 'thumb-' . $hash;
        
        /** @noinspection PhpUndefinedMethodInspection */
        $thumbnail->save($image->thumbnail);
        
        $image->save();
        
        if ( $this->resource ) {
            return new ImageResource($image);
        }
        
        return $image;
    }
}
