<?php

namespace App\Jobs\Images;

use App\Exceptions\ImageNotFound;
use App\Exceptions\UnauthorizedAccess;
use App\Http\Resources\Image as ImageResource;
use App\Repositories\ImageRepository;
use App\Traits\UserUtils;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Http\Request;

class ReadAll
{
    use Dispatchable, Queueable, UserUtils;
    
    /**
     * @var bool
     */
    private $resource;
    /**
     * @var \Illuminate\Http\Request
     */
    private $request;
    
    /**
     * ReadAll constructor.
     * @param \Illuminate\Http\Request $request
     * @param bool                     $resource
     */
    public function __construct( Request $request, $resource = true )
    {
        $this->resource = $resource;
        $this->request  = $request;
    }
    
    /**
     * @param \App\Repositories\ImageRepository $repository
     * @return \App\Http\Resources\Image|\App\Image
     * @throws \App\Exceptions\UnauthorizedAccess
     */
    public function handle( ImageRepository $repository )
    {
        $current = $this->currentUser();
        $images  = collect([]);
        $user    = $this->request->get('user_id');
        
        if ( !$current ) {
            throw new UnauthorizedAccess();
        }
        
        if ( $user && ( $current && $current->isAdmin() ) ) {
            $images = $repository->getAllByUserId($user);
        } else {
            if ( $current ) {
                $images = $repository->getAllByUserId($current->id);
            }
        }
        
        if ( $this->resource ) {
            return new ImageResource($images);
        }
        
        return $images;
    }
}
