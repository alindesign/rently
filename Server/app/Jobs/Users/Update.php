<?php

namespace App\Jobs\Users;

use App\Http\Resources\User as UserResource;
use App\Jobs\Images\Create as CreateImage;
use App\Jobs\Images\Destroy as DestroyImage;
use App\Repositories\UserRepository;
use App\Role;
use App\Traits\UserUtils;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

class Update
{
    use Queueable, DispatchesJobs, UserUtils;
    
    /**
     * @var bool
     */
    private $resource;
    /**
     * @var \Illuminate\Http\Request
     */
    private $request;
    private $id;
    
    /**
     * ReadAll constructor.
     * @param                                         $id
     * @param \Illuminate\Foundation\Http\FormRequest $request
     * @param bool                                    $resource
     */
    public function __construct( $id, FormRequest $request, $resource = true )
    {
        $this->request  = $request;
        $this->resource = $resource;
        $this->id       = $id;
    }
    
    /**
     * @param \App\Repositories\UserRepository $repository
     * @return \App\Http\Resources\User|\App\User
     */
    public function handle( UserRepository $repository )
    {
        $current = $this->currentUser();
        $user    = $repository->getById($this->id);
        
        if ( $this->request->get('first_name') ) {
            $user->first_name = $this->request->get('first_name');
        }
        
        if ( $this->request->get('last_name') ) {
            $user->last_name = $this->request->get('last_name');
        }
        
        if ( $this->request->get('email') ) {
            $user->email = $this->request->get('email');
        }
        
        if ( $this->request->get('phone') ) {
            $user->phone = $this->request->get('phone');
        }
        
        if ( $this->request->get('gender') ) {
            $user->gender = $this->request->get('gender');
        }
    
        if ( $this->request->get('gender') ) {
            $user->gender = $this->request->get('gender');
        }
        
        if ( $this->request->get('address') ) {
            $user->address = $this->request->get('address');
        }
        
        if (
            $this->request->get('password') === $this->request->get('password_confirmation') &&
            Hash::check($this->request->get('old_password'), $user->password)
        ) {
            $user->password = bcrypt($this->request->get('password'));
        }
        
        if ( $this->request->get('role') && $current && $current->isAdmin() ) {
            $user->role_id = intval($this->request->get('role'));
        }
        
        if ( $this->request->get('image') || $this->request->get('image') === 'delete' ) {
            if ( $user->image ) {
                $this->dispatch(new DestroyImage($user->image->id, $user->id, $this->resource));
            }
            
            if ( $this->request->get('image') ) {
                $img   = new CreateImage($this->request, $user->id, $this->resource);
                $image = $this->dispatch($img);
                
                $user->image_id = $image->id;
            }
        }
        
        $user->push();
        
        if ( $this->resource ) {
            return new UserResource($user);
        }
        
        return $user;
    }
}
