<?php

namespace App\Jobs\Users;

use App\Http\Resources\Users;
use App\Repositories\UserRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class ReadAll
{
    use Dispatchable, Queueable;
    
    /**
     * @var bool
     */
    private $resource;
    
    /**
     * ReadAll constructor.
     * @param bool $resource
     */
    public function __construct( $resource = true )
    {
        $this->resource = $resource;
    }
    
    /**
     * @param \App\Repositories\UserRepository $repository
     * @return \App\Http\Resources\Users|\App\User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function handle( UserRepository $repository )
    {
        $users = $repository->getAll();
        
        if ( $this->resource ) {
            return new Users($users);
        }
        
        return $users;
    }
}
