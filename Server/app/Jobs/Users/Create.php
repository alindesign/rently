<?php

namespace App\Jobs\Users;

use App\Http\Resources\User as UserResource;
use App\Jobs\Images\Create as CreateImage;
use App\Role;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Http\FormRequest;

class Create
{
    use Queueable, DispatchesJobs;
    
    /**
     * @var bool
     */
    private $resource;
    /**
     * @var \Illuminate\Http\Request
     */
    private $request;
    
    /**
     * ReadAll constructor.
     * @param \Illuminate\Foundation\Http\FormRequest $request
     * @param bool                                    $resource
     */
    public function __construct( FormRequest $request, $resource = true )
    {
        $this->request  = $request;
        $this->resource = $resource;
    }
    
    /**
     * @return \App\Http\Resources\User|\App\User
     */
    public function handle()
    {
        $user = new User();
        
        $user->first_name = $this->request->get('first_name');
        $user->last_name  = $this->request->get('last_name');
        $user->email      = $this->request->get('email');
        $user->phone      = $this->request->get('phone');
        $user->gender     = strtolower($this->request->get('gender'));
        $user->password   = bcrypt($this->request->get('password'));
        $user->role_id    = Role::CUSTOMER;
        
        $user->save();
        
        if ( $this->request->has('image') ) {
            $img   = new CreateImage($this->request, $user->id, $this->resource);
            $image = $this->dispatch($img);
            
            $user->update([
                'image_id' => $image->id
            ]);
        }
        
        if ( $this->resource ) {
            return new UserResource($user);
        }
        
        return $user;
    }
}
