<?php

namespace App\Jobs\Users;

use App\Exceptions\UserNotFound;
use App\Http\Resources\User;
use App\Repositories\UserRepository;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class Read
{
    use Dispatchable, Queueable;
    
    /**
     * @var bool
     */
    private $resource;
    private $id;
    
    /**
     * ReadAll constructor.
     * @param      $id
     * @param bool $resource
     */
    public function __construct( $id, $resource = true )
    {
        $this->resource = $resource;
        $this->id       = $id;
    }
    
    /**
     * @param \App\Repositories\UserRepository $repository
     * @return \App\Http\Resources\User|\App\User[]|\Illuminate\Database\Eloquent\Collection
     * @throws \App\Exceptions\UserNotFound
     */
    public function handle( UserRepository $repository )
    {
        $user = $repository->getById($this->id);
        
        if ( !$user ) {
            throw new UserNotFound();
        }
        
        if ( $this->resource ) {
            return new User($user);
        }
        
        return $user;
    }
}
