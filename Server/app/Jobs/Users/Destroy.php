<?php

namespace App\Jobs\Users;

use App\Exceptions\UserNotFound;
use App\Http\Resources\User;
use App\Repositories\UserRepository;
use App\Traits\UserUtils;
use Illuminate\Bus\Queueable;
use Illuminate\Foundation\Bus\Dispatchable;

class Destroy
{
    use Dispatchable, Queueable, UserUtils;
    
    /**
     * @var bool
     */
    private $resource;
    private $id;
    
    /**
     * ReadAll constructor.
     * @param      $id
     * @param bool $resource
     */
    public function __construct( $id, $resource = true )
    {
        $this->resource = $resource;
        $this->id       = $id;
    }
    
    /**
     * @param \App\Repositories\UserRepository $repository
     * @return \App\Http\Resources\User|\App\User
     * @throws \App\Exceptions\UserNotFound
     * @throws \Exception
     */
    public function handle( UserRepository $repository )
    {
        $current = $this->currentUser();
        $user    = $repository->getById($this->id);
        
        if ( !$user || $current || $current->id !== $user->id || !$user->isAdmin() ) {
            throw new UserNotFound();
        }
        
        if ( $this->resource ) {
            return new User($user);
        }
        
        $user->delete();
        
        return $user;
    }
}
