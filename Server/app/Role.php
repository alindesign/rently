<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property string name
 * @property string localized
 */
class Role extends Model
{
    const ADMIN      = 1;
    const ADMIN_NAME = 'Admin';
    
    const DEVELOPER      = 2;
    const DEVELOPER_NAME = 'Developer';
    
    const COMPANY      = 3;
    const COMPANY_NAME = 'Company';
    
    const CUSTOMER      = 4;
    const CUSTOMER_NAME = 'Customer';
    
    protected $table = 'roles';
    
    protected $fillable = [
        'name',
        'localize'
    ];
    
    static function  getRoles () {
        return [
            self::ADMIN_NAME,
            self::DEVELOPER_NAME,
            self::COMPANY_NAME,
            self::CUSTOMER_NAME
        ];
    }
}
