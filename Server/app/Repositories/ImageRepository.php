<?php
/**
 * Created by PhpStorm.
 * User: alind
 * Date: 14.12.2018
 * Time: 19:11
 */

namespace App\Repositories;


use App\Image;

class ImageRepository extends Repository
{
    /**
     * @return \App\Image[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return Image::all();
    }
    
    /**
     * @param int $id
     * @return Image
     */
    public function getById( int $id )
    {
        return Image::where('id', $id)->first();
    }
    
    /**
     * @param string $hash
     * @return Image
     */
    public function getByHash( string $hash )
    {
        return Image::where('hash', $hash)->first();
    }
    
    /**
     * @param int      $id
     * @param int|null $uid
     * @return Image
     */
    public function getByIdAndUser( int $id, int $uid )
    {
        return Image::where('id', $id)
                    ->where('user_id', $uid)
                    ->first();
    }
    
    /**
     * @param int $user
     * @return Image[]
     */
    public function getAllByUserId( int $user )
    {
        return Image::where('user_id', $user)->get();
    }
}
