<?php
/**
 * Created by PhpStorm.
 * User: alind
 * Date: 14.12.2018
 * Time: 19:11
 */

namespace App\Repositories;


use App\User;

class UserRepository extends Repository
{
    /**
     * @return \App\User[]|\Illuminate\Database\Eloquent\Collection
     */
    public function getAll()
    {
        return User::all();
    }
    
    /**
     * @param int $id
     * @return User
     */
    public function getById( int $id )
    {
        return User::where('id', $id)->first();
    }
    
    /**
     * @param string $email
     * @return User
     */
    public function getByEmail( string $email )
    {
        return User::where('email', $email)->first();
    }
}
