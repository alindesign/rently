<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "first_name" => "required|string",
            "password"   => "required|confirmed|min:6",
            "last_name"  => "string",
            "email"      => "email|unique:users",
            "phone"      => "unique:users",
            "gender"     => "in:male,female,Male,Female",
            "image"      => "file"
        ];
    }
}
