<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "first_name"            => "string",
            "last_name"             => "string",
            "old_password"          => "confirmed|min:6|",
            "password"              => "confirmed|min:6|different|old_password",
            "email"                 => "email|unique:users",
            "phone"                 => "unique:users",
            "gender"                => "in:male,female",
            "image"                 => "file"
        ];
    }
}
