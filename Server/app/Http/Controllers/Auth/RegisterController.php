<?php

namespace App\Http\Controllers\Auth;

use App\Http\Requests\CreateUser;
use App\Jobs\Users\Create;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    
    use RegistersUsers;
    
    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';
    
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    
    
    public function register( CreateUser $request )
    {
        $user = $this->create($request);
        event(new Registered($user));
        auth('web')->login($user);
        return $this->registered($request, $user) ? : redirect($this->redirectPath());
    }
    
    /**
     * Create a new user instance after a valid registration.
     *
     * @param \App\Http\Requests\CreateUser $request
     * @return \App\User
     */
    protected function create( CreateUser $request )
    {
        return $this->dispatch(new Create($request, false));
    }
}
