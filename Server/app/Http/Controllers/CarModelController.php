<?php

namespace App\Http\Controllers;

use App\CarDetail;
use Illuminate\Http\Request;

class CarModelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CarDetail $carModel
     * @return \Illuminate\Http\Response
     */
    public function show( CarDetail $carModel)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CarDetail $carModel
     * @return \Illuminate\Http\Response
     */
    public function edit( CarDetail $carModel)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \App\CarDetail           $carModel
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, CarDetail $carModel)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CarDetail $carModel
     * @return \Illuminate\Http\Response
     */
    public function destroy( CarDetail $carModel)
    {
        //
    }
}
