<?php

namespace App\Http\Controllers;

use App\Car;
use App\Exceptions\CarNotFound;
use App\Http\Requests\CreateCarRequest;
use App\Http\Resources\Car as CarResource;
use App\Jobs\Car\Create;
use App\Jobs\Car\Delete;
use App\Jobs\Car\Update;
use App\Price;
use App\Rent;
use App\Traits\Responder;
use App\Traits\UserUtils;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CarController extends Controller
{
    use DispatchesJobs, Responder, UserUtils;
    
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    public function index( Request $request )
    {
        
        Log::debug("get cars:", $request->all());
        $user         = $this->currentUser();
        $q            = trim($request->get("q"));
        $combustible  = trim(strtolower($request->get("combustible")));
        $transmission = trim(strtolower($request->get("transmission")));
        $own          = filter_var($request->get("own"), FILTER_VALIDATE_BOOLEAN);
        $promotions   = filter_var($request->get("promotions"), FILTER_VALIDATE_BOOLEAN);
        $history      = filter_var($request->get("history"), FILTER_VALIDATE_BOOLEAN);
        
        if ( $own ) {
            $query = Car::where("user_id", $user->id);
        } else {
            $query = Car::where('id', '>', 0);
        }
        
        if ( $promotions ) {
            $ids   = Price::where('discount', '>=', 0)->get()->pluck('car_id')->all();
            $query = $query->whereIn('id', $ids);
        }
        
        if ( $history ) {
            $ids   = Rent::where('user_id', $user->id)->get()->pluck('car_id')->all();
            $query = $query->whereIn('id', $ids);
        }
        
        $items = $query->orderBy('created_at', 'desc')->get();
        
        return $this->respond(
            $items->map(function ( $item ) {
                return new CarResource($item);
            })
                  ->reject(function ( $item ) use ( $request, $transmission, $combustible, $q ) {
                      $item = $item->toArray($request);
                
                      if ( $q && !str_contains($item[ 'name' ], $q) ) {
                          return true;
                      }
                
                      if ( $combustible && $combustible !== "all" && trim(strtolower($item[ 'combustible' ])) !== $combustible ) {
                          return true;
                      }
                
                      if ( $transmission && $transmission !== "all" && trim(strtolower($item[ 'transmission' ])) !== $transmission ) {
                          return true;
                      }
                
                      return false;
                  })
                  ->values()
        );
    }
    
    public function show( $id )
    {
        $car = Car::find($id);
        
        if ( !$car ) {
            throw new CarNotFound();
        }
        
        $car->views = intval($car->views) + 1;
        $car->save();
        
        return $this->respond(
            new CarResource($car)
        );
    }
    
    public function store( CreateCarRequest $request )
    {
        return $this->respond(
            $this->dispatchNow(
                new Create($request)
            )
        );
    }
    
    public function update( $id, CreateCarRequest $request )
    {
        return $this->respond(
            $this->dispatchNow(
                new Update($request, $id)
            )
        );
    }
    
    public function destroy( $id )
    {
        return $this->respond(
            $this->dispatchNow(
                new Delete($id)
            )
        );
    }
}
