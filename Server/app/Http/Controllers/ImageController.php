<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateImage;
use App\Jobs\Images\Create;
use App\Jobs\Images\Destroy;
use App\Jobs\Images\Read;
use App\Jobs\Images\ReadAll;
use App\Repositories\ImageRepository;
use App\Traits\Responder;
use App\Traits\UserUtils;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image as ImageManager;

class ImageController extends Controller
{
    use DispatchesJobs, Responder, UserUtils;

    /**
     * @var \App\Repositories\ImageRepository
     */
    private $repository;

    public function __construct(ImageRepository $repository)
    {
        $this->middleware('auth:api', [
            'except' => [
                'image'
            ]
        ]);
        $this->repository = $repository;
    }

    /**
     * Display a listing of the resource.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $this->dispatch(new ReadAll($request));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\CreateImage $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateImage $request)
    {
        $current = $this->currentUser();
        return $this->dispatch(new Create($request, $current->id));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Image              $image
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function show($image, Request $request)
    {
        $hash = $request->has('hash') ? $request->get('hash') : $image;

        if (!is_numeric($image) && $hash && $hash !== $image) {
            $image = null;
        }

        return $this->dispatch(new Read($image, $hash));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  $image
     * @return \Illuminate\Http\Response
     */
    public function destroy($image)
    {
        $current = $this->currentUser();

        return $this->dispatch(new Destroy($image, $current->id));
    }

    /**
     * @param                          $hash
     * @param \Illuminate\Http\Request $request
     */
    public function image($hash, Request $request)
    {
        $image = $this->repository->getByHash($hash);

        if (!$image) {
            return abort(404);
        }

        $resource = $request->has('thumb') || $request->has('thumbnail') ? $image->thumbnail : $image->path;

        if (!$resource) {
            return abort(404);
        }

        $img = ImageManager::make($resource);

        return $img->response();
    }
}
