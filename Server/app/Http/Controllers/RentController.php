<?php

namespace App\Http\Controllers;

use App\Exceptions\CarAlreadyRent;
use App\Exceptions\CarRentNotFound;
use App\Exceptions\CarRentUserDoesNotBelong;
use App\Http\Requests\CreateRentRequest;
use App\Rent;
use App\Traits\Responder;
use App\Traits\UserUtils;
use Carbon\Carbon;
use Illuminate\Http\Request;

class RentController extends Controller
{
    use UserUtils, Responder;
    
    /**
     * @param \App\Http\Requests\CreateRentRequest $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @throws \App\Exceptions\CarAlreadyRent
     */
    public function store( CreateRentRequest $request )
    {
        $user = $this->currentUser();
        
        $rents = Rent::where('car_id', $request->get("car_id"))->get();
        
        if ( $rents && $rents->count() > 0 ) {
            throw new CarAlreadyRent();
        }
        
        $rent = new Rent();
        
        $rent->user_id    = $user->id;
        $rent->car_id     = $request->get("car_id");
        $rent->address    = $request->get("address");
        $rent->rent_start = new Carbon($request->get("start_date"));
        $rent->rent_end   = new Carbon($request->get("end_date"));
        
        $rent->save();
        
        if ( $rent->car ) {
            $rent->car->active = 0;
            $rent->car->save();
        }
        
        return $this->respond($rent);
    }
    
    /**
     * @param                          $id
     * @param \Illuminate\Http\Request $request
     * @return \App\Rent
     * @throws \App\Exceptions\CarRentNotFound
     * @throws \App\Exceptions\CarRentUserDoesNotBelong
     */
    public function destroy( $id, Request $request )
    {
        $user = $this->currentUser();
        $rent = Rent::where('id', $id)->first();
        
        if ( !$rent ) {
            throw  new CarRentNotFound();
        }
        
        if ( $rent->user_id !== $user->id ) {
            throw  new CarRentUserDoesNotBelong();
        }
        
        if ( $rent->car ) {
            $rent->car->active = 1;
            $rent->car->save();
        }
        
        $rent->active = 0;
        $rent->save();
        
        return $rent;
    }
}
