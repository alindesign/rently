<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateUser;
use App\Http\Requests\UpdateUser;
use App\Jobs\Users\Create;
use App\Jobs\Users\ReadAll;
use App\Jobs\Users\Update;
use App\Jobs\Users\Read;
use App\Jobs\Users\Destroy;
use App\Repositories\UserRepository;
use App\Traits\Responder;
use Illuminate\Foundation\Bus\DispatchesJobs;

class UserController extends Controller
{
    use Responder, DispatchesJobs;
    
    /**
     * @var \App\Repositories\UserRepository
     */
    private $repository;
    
    /**
     * UserController constructor.
     * @param \App\Repositories\UserRepository $repository
     */
    public function __construct( UserRepository $repository )
    {
        $this->repository = $repository;
        
        $this->middleware('auth:api', [
            'except' => [
                'store'
            ]
        ]);
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return $this->respond(
            $this->dispatch(new ReadAll())
        );
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param \App\Http\Requests\CreateUser $request
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function store( CreateUser $request )
    {
        return $this->respond(
            $this->dispatch(new Create($request))
        );
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function show( $id )
    {
        return $this->respond(
            $this->dispatch(new Read($id))
        );
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param \App\Http\Requests\UpdateUser $request
     * @param  int                          $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function update( UpdateUser $request, $id )
    {
        return $this->respond(
            $this->dispatch(new Update($id, $request))
        );
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function destroy( $id )
    {
        return $this->respond(
            $this->dispatch(new Destroy($id))
        );
    }
}
