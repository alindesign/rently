<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

/**
 * @property mixed id
 * @property mixed email
 * @property mixed first_name
 * @property mixed last_name
 * @property mixed gender
 * @property mixed phone
 * @property mixed email_verified_at
 * @property mixed role
 * @property mixed image
 * @property mixed company
 */
class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray( $request )
    {
        return [
            'name'       => $this->name(),
            'id'         => $this->id,
            'email'      => $this->email,
            'first_name' => $this->first_name,
            'last_name'  => $this->last_name,
            'phone'      => $this->phone,
            'address'    => $this->address,
            'gender'     => ucfirst($this->gender),
            'active'     => !is_null($this->email_verified_at),
            'role'       => $this->role ? $this->role->name : null,
            'thumbnail'  => $this->image ? route('image', [
                'hash' => $this->image->hash
            ]) : null,
            'image'      => $this->image,
            'company'    => $this->company
        ];
    }
}
