<?php

namespace App\Http\Resources;

use App\Traits\UserUtils;
use Illuminate\Http\Resources\Json\JsonResource;

class Car extends JsonResource
{
    use UserUtils;
    
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray( $request )
    {
        $user         = $this->currentUser();
        $image        = $this->image;
        $name         = '';
        $class        = '';
        $year         = '';
        $transmission = '';
        $engine       = '';
        $fullImage    = null;
        $thumbnail    = null;
        $vehicle      = null;
        
        $rent   = $this->rent($user->id);
        $rents  = $this->rents($user->id);
        $price  = $this->price;
        $detail = $this->detail;
        
        if ( $detail ) {
            $vehicle      = $detail->vehicle;
            $name         = implode(' ', [
                $vehicle->make->name,
                $vehicle->model->name
            ]);
            $class        = $vehicle->model->class;
            $year         = $vehicle->year->year;
            $transmission = $vehicle->transmission;
            $engine       = $vehicle->name;
        }
        
        if ( $image ) {
            $fullImage = route('image', [
                'hash' => $this->image->hash
            ]);
            
            $thumbnail = "$fullImage?thumbnail";
        }
        
        $currency      = $price ? $price->currency : 'RON';
        $realPrice     = $price ? $price->price : 0;
        $discount      = $price ? $price->discount : 0;
        $discountPrice = $realPrice - ( ( $realPrice * $discount ) / 100 );
        
        return [
            'id'              => $this->id,
            'own'             => $this->user_id === $user->id,
            'kilometers'      => $this->kilometers,
            'description'     => $this->description,
            'combustible'     => ucfirst($this->combustible),
            'active'          => intval($this->active) === 1,
            'doors'           => $this->doors,
            'rent_by_me'      => !!$rent,
            'rent_count'      => $rents ? $rents->count() : 0,
            'rent_id'         => $rent ? $rent->id : null,
            'ac'              => $this->ac === 1,
            'color'           => ucfirst($this->color),
            'views'           => intval($this->views),
            'views_text'      => $this->views . " views",
            'real_price'      => $realPrice,
            'real_price_text' => $realPrice > 0 ? $realPrice . " " . $currency : "Free",
            'price'           => $discountPrice,
            'discount'        => $discount,
            'discount_text'   => $discount . "%",
            'price_text'      => $discountPrice > 0 ? $discountPrice . " " . $currency : 'Free',
            'name'            => $name,
            'engine'          => $engine,
            'transmission'    => $transmission,
            'year'            => $year,
            'class'           => $class,
            'image'           => $fullImage,
            'thumbnail'       => $thumbnail
        ];
    }
}
