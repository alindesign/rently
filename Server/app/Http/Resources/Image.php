<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Image extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $image = route('image', ['hash' => $this->hash]);

        return [
            "id" => $this->id,
            "name" => $this->name,
            "description" => $this->description,
            "filename" => $this->name,
            "size" => $this->size,
            "mime_type" => $this->mime_type,
            "extension" => $this->extension,
            "user_id" => $this->user_id,
            "hash" => $this->hash,
            "path" => $this->path,
            "image" => $image,
            "thumbnail_path" => $this->thumbnail,
            "thumbnail" => $image . '?thumbnail',
            "created_at" => $this->created_at,
            "updated_at" => $this->updated_at
        ];
    }
}
