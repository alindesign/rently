<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    const TRANSMISSION_MANUAL   = 'manual';
    const TRANSMISSION_AUTOMATE = 'automate';
    
    const COMBUSTIBLE_DIESEL      = "Diesel";
    const COMBUSTIBLE_GASOLINE    = "Gasoline";
    const COMBUSTIBLE_LPG         = "LPG";
    const COMBUSTIBLE_ELECTRICITY = "Electricity";
    
    protected $table = 'cars';
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function image()
    {
        return $this->belongsTo(Image::class, 'image_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function price()
    {
        return $this->hasOne(Price::class);
    }
    
    /**
     * @param $userId
     * @return array
     */
    public function rent( $userId )
    {
        $rent = Rent::where('car_id', $this->id)
                    ->where('user_id', $userId)
                    ->where('active', 1)
                    ->orderBy('created_at', 'desc')
                    ->first();
        
        return $rent;
    }
    
    public function rents( $userId )
    {
        $rent = Rent::where('car_id', $this->id)
                    ->where('user_id', $userId)
                    ->get();
        
        return $rent;
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function company()
    {
        return $this->belongsTo(Company::class, 'company_id', 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function detail()
    {
        return $this->belongsTo(CarDetail::class, 'car_detail_id', 'id');
    }
}
