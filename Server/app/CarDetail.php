<?php

namespace App;

use Gerardojbaez\Vehicle\Contracts\HasMake as HasMakeContract;
use Gerardojbaez\Vehicle\Contracts\HasModel as HasModelContract;
use Gerardojbaez\Vehicle\Contracts\HasVehicle as HasVehicleContract;
use Gerardojbaez\Vehicle\Traits\HasMake;
use Gerardojbaez\Vehicle\Traits\HasModel;
use Gerardojbaez\Vehicle\Traits\HasModelYear;
use Gerardojbaez\Vehicle\Traits\HasVehicle;
use Illuminate\Database\Eloquent\Model;

class CarDetail extends Model implements HasMakeContract, HasModelContract, HasVehicleContract
{
    use HasMake, HasModel, HasModelYear, HasVehicle;


    protected $table = 'car_details';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'make_id',
        'model_id',
        'year_id',
        'vehicle_id'
    ];
}
