<?php
/**
 * Created by PhpStorm.
 * User: alind
 * Date: 14.12.2018
 * Time: 19:19
 */

namespace App\Traits;


use Illuminate\Http\Response;

trait Responder
{
    /**
     * @param array $data
     * @param int   $status
     * @param array $headers
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function respondWith( $data = [], $status = Response::HTTP_OK, $headers = [] )
    {
        return response()->json($data, $status, $headers);
    }
    
    /**
     * @param       $data
     * @param array $extra
     * @param int   $status
     * @param array $headers
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function respond( $data, $extra = [], $status = Response::HTTP_OK, $headers = [] )
    {
        return $this->respondWith(
            array_merge($extra, [
                'data' => $data
            ]),
            $status,
            $headers
        );
    }
    
    /**
     * @param       $data
     * @param array $extra
     * @param int   $status
     * @param array $headers
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function respondError( $data, $extra = [], $status = Response::HTTP_INTERNAL_SERVER_ERROR, $headers = [] )
    {
        return $this->respondWith(
            array_merge($extra, [
                'error' => $data
            ]),
            $status,
            $headers
        );
    }
    
    /**
     * @param       $data
     * @param array $extra
     * @param array $headers
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function respondForbidden( $data, $extra = [], $headers = [] )
    {
        return $this->respondError($data, $extra, Response::HTTP_FORBIDDEN, $headers);
    }
    
    /**
     * @param       $data
     * @param array $extra
     * @param array $headers
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function respondInternalServerError( $data, $extra = [], $headers = [] )
    {
        return $this->respondError($data, $extra, Response::HTTP_INTERNAL_SERVER_ERROR, $headers);
    }
    
    /**
     * @param       $data
     * @param array $extra
     * @param array $headers
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function respondUnauthorized( $data, $extra = [], $headers = [] )
    {
        return $this->respondError($data, $extra, Response::HTTP_UNAUTHORIZED, $headers);
    }
    
    /**
     * @param \Exception $exception
     * @param array      $extra
     * @param array      $headers
     * @return \Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     */
    public function respondException( \Exception $exception, $extra = [], $headers = [] )
    {
        return $this->respondError($exception->getMessage(), array_merge([
            'status' => $exception->getCode()
        ], $extra), $exception->getCode(), $headers);
    }
}
