<?php
/**
 * Created by PhpStorm.
 * User: alind
 * Date: 14.12.2018
 * Time: 19:05
 */

namespace App\Traits;


trait UserUtils
{
    /**
     * @return \App\User|null
     */
    public function currentUser()
    {
        return auth('api')->user();
    }
}
