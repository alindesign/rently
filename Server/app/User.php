<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as LaravelUser;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * @property int    role_id
 * @property string password
 * @property string gender
 * @property string phone
 * @property string email
 * @property string last_name
 * @property string first_name
 * @property int    image_id
 * @property int    id
 */
class User extends LaravelUser implements JWTSubject
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'email',
        'password',
        'first_name',
        'last_name',
        'phone',
        'gender',
        'email_verified_at',
        'role_id',
        'image_id',
        'company_id'
    ];
    
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token'
    ];
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function role()
    {
        return $this->hasOne(Role::class, 'id');
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function company()
    {
        return $this->hasOne(Company::class);
    }
    
    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function image()
    {
        return $this->hasOne(Image::class);
    }
    
    /**
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    
    /**
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }
    
    /**
     * @return string
     */
    public function name()
    {
        return trim(( $this->first_name ?? '' ) . ' ' . ( $this->last_name ?? '' ));
    }
    
    /**
     * @return string
     */
    public function full_name()
    {
        return $this->name();
    }
    
    public function _isRole( $role )
    {
        return $this->role && $this->role->id === $role;
    }
    
    /**
     * @return bool
     */
    public function isAdmin()
    {
        return $this->_isRole(Role::ADMIN);
    }
    
    /**
     * @return bool
     */
    public function isDeveloper()
    {
        return $this->_isRole(Role::DEVELOPER);
    }
    
    /**
     * @return bool
     */
    public function isCompany()
    {
        return $this->_isRole(Role::COMPANY);
    }
    
    /**
     * @return bool
     */
    public function isCustomer()
    {
        return $this->_isRole(Role::CUSTOMER);
    }
    
}
