<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed user_id
 * @property mixed car_id
 * @property mixed address
 * @property mixed rent_start
 * @property mixed rent_end
 */
class Rent extends Model
{
    protected $table = "rents";
    
    public function car()
    {
        return $this->belongsTo(Car::class, 'car_id', 'id');
    }
    
    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }
    
}
