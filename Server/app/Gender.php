<?php
/**
 * Created by PhpStorm.
 * User: alind
 * Date: 12.12.2018
 * Time: 20:32
 */

namespace App;


class Gender
{
    const MALE   = 'male';
    const FEMALE = 'female';
}
