<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>{{ config('app.name', 'Rently') }}</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">

        <script src="//code.jquery.com/jquery.min.js"></script>

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                min-height: 100vh;
                margin: 0;
            }

            body {
                background: #fff url({{ asset('/images/background.jpg') }}) center center no-repeat;
                -webkit-background-size: cover;
                background-size: cover;
                min-width: 960px;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: flex-start;
                align-content: flex-start;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .content {
                text-align: center;
                margin: 30px 15px 0;
                width: calc(100% - 30px);
                max-width: 1200px;
                min-width: 930px;
                z-index: 5;
            }

            .content-bg {
                position: absolute;
                z-index: 3;
                background-color: #fff;
                width: 100%;
                height: 240px;
                -webkit-box-shadow: 0 0 20px rgba(0, 0, 0, .2);
                box-shadow: 0 0 20px rgba(0, 0, 0, .2);
            }

            .title {
                display: inline-block;
            }

            .links {
                padding: 20px 0 40px;
            }

            .links > a {
                color: #3d3642;
                padding: 0 25px;
                font-size: 14px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .links > a:hover {
                color: #78737b;
            }

            .m-b-md {
                margin-bottom: 20px;
            }

            .screenshots img {
                width: 100%;
                height: auto;
                -webkit-transition: all 250ms;
                transition: all 250ms;
                -webkit-box-shadow: 0 0 20px rgba(0, 0, 0, .2);
                box-shadow: 0 0 20px rgba(0, 0, 0, .2);
            }

            .img {
                display: inline-block;
                position: relative;
                width: 100%;
                min-height: 680px;
                border-radius: 5px;
                overflow: hidden;
                background-color: #fff;
            }

            .img .screenshot {
                position: absolute;
                top: 0;
                left: 50%;
                -webkit-transform: translate(-50%, 0);
                transform: translate(-50%, 0);
            }
        </style>
    </head>
    <body>
        <div class="flex-center position-ref full-height">
            <div class="content-bg"></div>
            <div class="content">
                <header>
                    <a href="{{ url('/') }}" class="title m-b-md">
                        <img src="{{ asset('images/logo.png') }}" alt="{{ config('app.name', 'Rently') }}">
                    </a>

                    <div class="links">
                        <a download href="{{ url('/download/application.zip') }}">Download</a>
                        <a href="{{ route('register') }}">Sign up</a>
                        <a href="{{ route('login') }}">Sign in</a>
                        <a href="{{ url('/about') }}">About</a>
                        <a href="{{ url('/terms') }}">Terms & Conditions</a>
                        <a href="https://bitbucket.org/alindesign/rently">Git source</a>
                    </div>
                </header>

                <div class="screenshots">
                    <a href="javascript:void(0)" onclick="next()" class="img">
                        <img class="screenshot" src="{{ asset('images/screenshots/01.jpg') }}" alt="List Page">
                    </a>
                    <script>
                        var last_timeout = null;
                        var list         = [
                            '{{asset('images/screenshots/01.jpg')}}',
                            '{{asset('images/screenshots/02.jpg')}}',
                            '{{asset('images/screenshots/03.jpg')}}',
                        ];

                        function next () {
                            if ( last_timeout ) {
                                clearTimeout(last_timeout);
                                last_timeout = null;
                            }

                            var item  = list.shift();
                            var img   = $('.screenshot');
                            var clone = img.clone();

                            clone.attr('src', item);
                            clone.css({
                                opacity: 0,
                                width:   '100%',
                                height:  'auto'
                            });
                            clone.on('load', function () {
                                img.css({
                                    opacity: 0,
                                    width:   '100%',
                                    height:  'auto'
                                });

                                setTimeout(function () {
                                    img.remove();
                                    clone.css({
                                        opacity: 1,
                                        width:   '100%',
                                        height:  'auto'
                                    });

                                    last_timeout = setTimeout(() => {
                                        last_timeout = null;
                                        next();
                                    }, 6000);
                                }, 250);
                            });

                            img.parent().append(clone);

                            list.push(item);
                        }

                        $(document).ready(function () {
                            next();
                        });
                    </script>
                </div>
            </div>
        </div>
    </body>
</html>
